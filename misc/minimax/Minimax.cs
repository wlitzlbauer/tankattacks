using System;
using System.Collections;

namespace Minimax
{
	class Game
	{
		struct Position
		{
			public int x;
			public int y;

			public Position(int x, int y)
			{
				this.x = x;
				this.y = y;
			}
		}

		struct Move
		{
			public int player;
			public Position position;

			public Move(int player, Position position)
			{
				this.player = player;
				this.position = position;
			}

			public Boolean IsValid()
			{
				return this.player != -1;
			}
		}

		static Move NONE = new Move(-1, new Position(-1, -1));

		// datastructure used for minimax
		struct Result
		{
			public int bestScore;
			public Move bestMove;

			public Result(int bestScore, Move bestMove)
			{
				this.bestScore = bestScore;
				this.bestMove = bestMove;
			}
		}

		class Board
		{
			// static array with all row combinations in the game.
			private static int[,] ROWS = new int[,]
			{
				{ 0, 1, 2 },
				{ 3, 4, 5 },
				{ 6, 7, 8 },
				{ 0, 3, 6 },
				{ 1, 4, 7 },
				{ 2, 5, 8 },
				{ 0, 4, 8 },
				{ 2, 4, 6 }
			};

			private const int CELL_WIDTH = 3;
			private const int CELL_HEIGHT = 3;
			private const int PLAYER_COUNT = 2;

			private int moveCount = 0;
			private int currentPlayer = 0;
			private int winner = -1;
			private int[] cells = new int[CELL_WIDTH * CELL_HEIGHT];

			public Board()
			{
				for (int i = 0; i < cells.Length; i++)
				{
					cells[i] = -1;
				}
			}

			public Board Clone()
			{
				Board board = new Board();
				board.currentPlayer = currentPlayer;
				board.moveCount = moveCount;
				Array.Copy(cells, board.cells, cells.Length);
				return board;
			}

			public ArrayList GetMoves()
			{
				ArrayList list = new ArrayList();

				for (int i = 0; i < cells.Length; i++)
				{
					if (cells[i] < 0)
					{
						int x = i % CELL_WIDTH;
						int y = i / CELL_WIDTH;
						list.Add(new Move(currentPlayer, new Position(x, y)));
					}
				}
				
				return list;
			}

			public Board MakeMove(Move move)
			{
				Board newBoard = Clone();

				if (move.IsValid())
				{
					newBoard.cells[move.position.x + move.position.y * CELL_WIDTH] = move.player;
				}

				newBoard.moveCount++;
				newBoard.currentPlayer = (currentPlayer + 1) % PLAYER_COUNT;

				return newBoard;
			}

			// Helper function
			// Returns the amount of cells owned by the player in the given row.
			private int GetSum(int row, int player)
			{
				int sum = 0;

				for (int i = 0; i < 3; i++)
				{
					int value = cells[ROWS[row, i]];

					if (value == player)
					{
						sum++;
					}
				}

				return sum;
			}

			public int Evaluate(int player)
			{
				return 0;
			}

			public int CurrentPlayer()
			{
				return currentPlayer;
			}

			public int GetWinner()
			{
				return winner;
			}

			public Boolean IsGameOver()
			{
				bool gameOver = moveCount >= 9;

				for (int i = 0; i < 8; i++)
				{
					int sumPlayer1 = GetSum(i, 0);
					int sumPlayer2 = GetSum(i, 1);

					if (sumPlayer1 == 3)
					{
						winner = 0;
						return true;
					}

					if (sumPlayer2 == 3)
					{
						winner = 1;
						return true;
					}
				}
				return gameOver;
			}

			public void Render()
			{
				Console.WriteLine("Round: " + moveCount);

				for (int y = 0; y < CELL_HEIGHT; y++)
				{
					String line = "";
					for (int x = 0; x < CELL_WIDTH; x++)
					{
						int player = cells[x + y * CELL_WIDTH];

						if (player >= 0)
						{
							line += player;
						}
						else
						{
							line += "-";
						}
					}
					Console.WriteLine(line);
				}
			}
		}

		private static Result Minimax(Board board, int player, int maxDepth, int currentDepth)
		{
			return new Result(board.Evaluate(player), new Move(player, new Position(0, 0)));
		}

		private static Move getBestMove(Board board, int player, int maxDepth)
		{
			return Minimax(board, player, maxDepth, 0).bestMove;
		}

		public static void Main(string[] args)
		{
			Board board = new Board();

			while (!board.IsGameOver())
			{
				Move move;

				if (board.CurrentPlayer() == 0)
				{
					board.Render();
					Console.WriteLine("Enter move: x,y");
					string input = Console.ReadLine();
					string[] values = input.Split(new char[] { ',', '-', ' ' });
					int x = Convert.ToInt32(values[0]);
					int y = Convert.ToInt32(values[1]);
					Console.WriteLine("Make move: " + x + " " + y);

					move = new Move(board.CurrentPlayer(), new Position(x, y));
				}
				else
				{
					move = getBestMove(board, 0, 9);
				}
				board = board.MakeMove(move);
			}

			board.Render();
			Console.WriteLine("The winner is: " + board.GetWinner());
			Console.ReadLine();
		}
	}
}

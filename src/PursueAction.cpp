#include "stdafx.h"
#include "GameDefines.h"
#include "PursueAction.h"
#include "TankAI.h"
#include "Tank.h"
#include "WorldInterface.h"

PursueAction::PursueAction(TankAI* _ai, Tank* _evader):
    Action(_ai),
    evader(_evader)
{
}

void PursueAction::update(float delta)
{
	TankAI* ai = getAI();
	Tank* tank = getTank();
	
	Tank::Steering steering;
	
	if (evader != NULL)
	{
		steering.linear = ai->pursue(evader);
	}
	
	steering.angular = ai->lookForward(tank->getLinearVelocity() + steering.linear);
	steering.aiming = ai->aimForward(tank->getLinearVelocity() + steering.linear);
	
	tank->setSteering(steering);
}

#ifndef __WORLD_INTERFACE_H__
#define __WORLD_INTERFACE_H__

#include <vector>

#include "World.h"

class Tank;

/// Interface to perceive the world.
/// Note: The AI shouldn't access the world directly.
///       If it needs more information about the world expose it here.
class WorldInterface
{
public:
	static WorldInterface* getInstance();
	
	/// Returns all Tanks within the given radius arround the given tank.
	/// Note: result is not sorted.
	void getTanksNearby(Tank* myTank, float radius, std::vector<Tank*>& result) const;
	
	/// Returns all Tanks in the game world.
	void getAllTanks(std::vector<Tank*>& result) const;
	
	/// Returns the Tank at the given position.
	Tank* getTankAt(const Vec2f& position) const;

	/// Performes a raycast and sets the given CollisionContact to the nearest collision.
	/// @return true if collision is found
	bool rayCast(const Vec2f& p1, const Vec2f& p2, CollisionContact& collision, World::Filter filter = World::FILTER_ALL) const
	{
		World* world = World::getInstance();
		return world->rayCast(p1, p2, collision, filter);
	}
    
	/// Returns the tank of the player
    Tank* getPlayerTank() const
    {
        return playerTank;
    }
	
	void setPlayerTank(Tank* _playerTank)
    {
        playerTank = _playerTank;
    }
	
	float getDistanceToPlayer(Tank* tank);
	
private:
	WorldInterface();
	
	static WorldInterface* instance;

    Tank* playerTank;
};


#endif


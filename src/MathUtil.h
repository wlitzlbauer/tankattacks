#ifndef __MATH_UTIL__
#define __MATH_UTIL__

/// Collection of useful mathematical function and constants.
class MathUtil
{
public:
	/// PI (180°)
	static const float PI;
	
	/// Double PI (360°)
	static const float PI_2;
	
	/// Half PI (90°)
	static const float PI_HALF;
	
	/// Quarter PI (45°)
	static const float PI_QUARTER;
	
	/// Converst radiant to degree
	/// degree = radiant * RADIANT_TO_DEGREE
	static const float RADIANT_TO_DEGREE;
	
	/// Converst degree to radiant
	/// radiant = degree * DEGREE_TO_RADIANT	
	static const float DEGREE_TO_RADIANT;	
	
	/// Rounding error when using floats.
	/// Can be used to check for 0.0f.
	static const float ROUNDING_ERROR;
	
	/// Clamp the given value between min and max:
	/// result >= min and result <= max
	template<class T>
	static T clamp(T a, T min = 0, T max = 1)
	{
		return (a < min) ? min : (a > max) ? max : a;
	}

	/// Returns the higher value
	template<class T>
	static T max(T a, T b)
	{
		return (a > b) ? a : b;
	}
	
	/// Returns the lower value
	template<class T>
	static T min(T a, T b)
	{
		return (a < b) ? a : b;
	}

	template<class T>
	static T round(T r) 
	{
		return (r > (T) 0.0) ? floor(r + (T) 0.5) : ceil(r - (T) 0.5);
	}
	
	/// Simple power of 2 function
	/// value * value
	template<class T>
	static T pow(T a)
	{
		return a * a;
	}
	
	/// Returns the signum of the given value.
	/// The signum is defined as: 
	///  1 if the value is positive,
	/// -1 if the value is negative
	///  0 if the value is 0.
	static int signum(int a)
	{
		return (a > 0) ? 1: ((a < 0) ? -1 : 0);
	}
	
	static float signum(float a)
	{
		return (a > 0.0f) ? 1.0f : ((a < 0.0f) ? -1.0f : 0.0f);
	}
	
	static double signum(double a)
	{
		return (a > 0.0) ? 1.0 : ((a < 0.0) ? -10.0 : 0.0);
	}
	
	/// Returns a random int number where:
	/// x >= 0 and 
	/// x < max
	static int random(int max);
	
	/// Returns a binomial random numer:
	/// return rand + rand
	static float randomBinomial();
	
	static float mapToRange(float angle);
};

#endif


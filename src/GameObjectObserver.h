
#ifndef __GAME_OBJECT_OBSERVER_H__
#define __GAME_OBJECT_OBSERVER_H__

class GameObject;

class GameObjectObserver
{
public:
    virtual ~GameObjectObserver() {};
    virtual void onCollision(GameObject* collObject) = 0;
};

#endif

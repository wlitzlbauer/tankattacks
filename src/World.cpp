#include "stdafx.h"
#include "GameDefines.h"
#include <assert.h>
#include "World.h"
#include "NavigationGraph.h"
#include "GameObject.h"
#include "MathUtil.h"
#include "PhysicsDebugDraw.h"
#include "RigidBody.h"
#include "Box2DUtil.h"


World* World::instance = NULL;

World* World::getInstance()
{
	if (instance == NULL)
	{
		instance = new World();
	}
	
	return instance;
}

void World::ContactListener::BeginContact(b2Contact* contact)
{
	GameObject* objA = (GameObject*) contact->GetFixtureA()->GetBody()->GetUserData();
	GameObject* objB = (GameObject*) contact->GetFixtureB()->GetBody()->GetUserData();
	
	if ((objA != NULL) && (objB != NULL))
	{

		objA->onCollision(objB);
		objB->onCollision(objA);
	}
}

void World::ContactListener::EndContact(b2Contact* contact)
{}

World::World():
	remainingTime(0.0f)
{
	navigationGraph = new NavigationGraph(WORLD_WIDTH, WORLD_HEIGHT);
	
	// top-down view. there is no gravity.
	b2Vec2 gravity(0.0f, 0.0f);
	physicsWorld = new b2World(gravity);
	
	physicsDebugDraw = new PhysicsDebugDraw();
	physicsWorld->SetDebugDraw(physicsDebugDraw);
	physicsDebugDraw->SetFlags( b2Draw::e_shapeBit );
	
	contactListener = new ContactListener(this);
	physicsWorld->SetContactListener(contactListener);
}

World::~World()
{
	delete contactListener;
	delete physicsWorld;
	delete physicsDebugDraw;
	delete navigationGraph;
}

RigidBody* World::createDynamicCircle(const Vec2f& position, float radius, float friction)
{
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.position.Set(position.x, position.y);
	
	b2Body *body = physicsWorld->CreateBody( &bodyDef );
	
	b2CircleShape dynamicBox;
	dynamicBox.m_radius = radius;
	
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &dynamicBox;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = friction;
	fixtureDef.restitution = 0.5f;
	fixtureDef.filter.categoryBits = FILTER_DYNAMIC;
	
	body->CreateFixture( &fixtureDef );
	
	return new RigidBody(body);
}

RigidBody* World::createDynamicBox(const Vec2f& position, const Vec2f& extends, float friction)
{
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.position.Set(position.x, position.y);
	
	b2Body *body = physicsWorld->CreateBody( &bodyDef );
	
	b2PolygonShape dynamicBox;
	dynamicBox.SetAsBox(extends.x, extends.y);
	
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &dynamicBox;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = friction;
	fixtureDef.restitution = 0.5f; // bounce
	fixtureDef.filter.categoryBits = FILTER_DYNAMIC;
	
	body->CreateFixture( &fixtureDef );
	
	return new RigidBody(body);
}

RigidBody* World::createStaticBox(const Vec2f& position, const Vec2f& extends)
{
	b2BodyDef bodyDef;
	bodyDef.type = b2_staticBody;
	bodyDef.position.Set(position.x, position.y);
	
	b2Body *body = physicsWorld->CreateBody( &bodyDef );
	
	b2PolygonShape dynamicBox;
	dynamicBox.SetAsBox(extends.x, extends.y);
	
	body->CreateFixture( &dynamicBox, 0.0f );

	return new RigidBody(body);
}


bool World::queryAABB(const Vec2f& position, const Rectf& rectangle) const
{
	b2AABB aabb;
	aabb.lowerBound.Set(position.x + rectangle.x1, position.y + rectangle.y1);
	aabb.upperBound.Set(position.x + rectangle.x2, position.y + rectangle.y2);

	class Callback: public b2QueryCallback
	{
	public:
		bool hasCollision;

		Callback():
			hasCollision(false)
		{}

		bool ReportFixture(b2Fixture* fixture)
		{
			hasCollision = true;
			return true;
		}
	};

	Callback callback;

	physicsWorld->QueryAABB(&callback, aabb);

	return callback.hasCollision;
}

bool World::rayCast(const Vec2f& p1, const Vec2f& p2, Filter filter) const
{
	CollisionContact contact;
	return rayCast(p1, p2, contact);
}

bool World::rayCast(const Vec2f& p1, const Vec2f& p2, CollisionContact& collision, Filter filter) const
{
	if (p1.distanceSquared(p2) == 0.0f)
	{
		return false;
	}

	class Callback: public b2RayCastCallback
	{
	public:
		CollisionContact& result;
		uint16 filter;
		bool hasCollision;
		
		Callback(CollisionContact& _result, uint16 _filter):
			result(_result),
			filter(_filter),
			hasCollision(false)
		{}
		
		float32 ReportFixture(b2Fixture* fixture, const b2Vec2& point,
							  const b2Vec2& normal, float32 fraction)
		{
			if ((fixture->GetFilterData().categoryBits & filter) == fixture->GetFilterData().categoryBits)
			{
				result.point = Box2DUtil::convert(point);
				result.normal = Box2DUtil::convert(normal);
				hasCollision = true;
				return fraction;
			}
			
			return -1;
		}
	};
	
	Callback callback(collision, (uint16) filter);
	physicsWorld->RayCast(&callback, Box2DUtil::convert(p1), Box2DUtil::convert(p2));
	
	return callback.hasCollision;
}


void World::updatePhysics(float delta)
{
	// updates the physics engine with a fixed timestamp.
	
	float timestep = 1.0f / 60.0f;
	remainingTime += delta;
	
	while (remainingTime >= timestep)
	{
		physicsWorld->Step(timestep, 10, 10);
		remainingTime -= timestep;
	}
}


void World::update(float delta)
{
	// update physics
	updatePhysics(delta);
	
	// update game objects
	for (int i = 0; i < (int) objects.size(); i++)
	{
		objects[i]->update(delta);
	}
    
	// delete dead game objects
    for (int i = 0; i < (int) deleteList.size(); i++)
    {
        std::vector<GameObject*>::iterator it = std::find(objects.begin(), objects.end(), deleteList[i]);

        if (it != objects.end())
        {
            delete (*it);
            objects.erase(it);
        }
    }
    
    deleteList.clear();
}

void World::draw() const
{
	for (int i = 0; i < (int) objects.size(); i++)
	{
		objects[i]->draw();
	}
}

void World::debugDraw() const
{
	for (int i = 0; i < (int) objects.size(); i++)
	{
		objects[i]->debugDraw();
	}
}

void World::debugDrawPhysics() const
{
	physicsWorld->DrawDebugData();
}

void World::addObject(GameObject* object)
{
	objects.push_back(object);
}

void World::addForDelete(GameObject* object)
{
    deleteList.push_back(object);
}


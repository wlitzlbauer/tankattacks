#ifndef __WORLD_H__
#define __WORLD_H__

#include <vector>
#include <set>
#include <map>

#include "Box2D/Box2D.h"

class NavigationGraph;
class GameObject;
class Shape;
class PhysicsDebugDraw;
class RigidBody;

/// The World manages and renders all GameObjects in the game.
/// It is also used for creating new RigidBodies and
/// updates the underlaying physics engine (Box2D)
class World
{
public:
	// local class used internally for collision listening.
	class ContactListener : public b2ContactListener
	{
	public:
		ContactListener(World* _owner):
			owner(_owner)
		{}
		
		virtual ~ContactListener() {}
		
		void BeginContact(b2Contact* contact);
		void EndContact(b2Contact* contact);
		
	private:
		World* owner;
	};
	
	enum Filter
	{
		FILTER_STATIC = 0x0001,
		FILTER_DYNAMIC = 0x0002,

		FILTER_ALL = 0xFFFF
	};
	
	static const int WORLD_WIDTH = 800;
	static const int WORLD_HEIGHT = 600;
	
	static World* getInstance();
	
	virtual ~World();
	
	void update(float delta);
	void draw() const;
	
	/// renderes debug draw of the world.
	void debugDraw() const;
	
	/// renderes debug draw of the physics.
	void debugDrawPhysics() const;
	
	/// Adds a new GameObject to the world.
	void addObject(GameObject* object);
	
	/// Marks a GameObject for deletion.
	/// The object will get deleted after the update loop of the game objects.
    void addForDelete(GameObject* object);
	
	/// Checks if the given AABB is collising on the given position
	/// @return true if collision is found.
	bool queryAABB(const Vec2f& position, const Rectf& rectangle) const;

	/// Performes a raycast.
	/// Returns true if a obstacle was found.
	bool rayCast(const Vec2f& p1, const Vec2f& p2, Filter = FILTER_ALL) const;
	
	/// Performes a raycast and sets the given CollisionContact to the nearest collision.
	/// @return true if collision is found
	bool rayCast(const Vec2f& p1, const Vec2f& p2, CollisionContact& collision, Filter = FILTER_ALL) const;
	
	/// Returns the navigatin graph
	NavigationGraph* getNavigationGraph() 
	{ 
		return navigationGraph; 
	}
	
	/// Returns a reference to the GameObject list
	const std::vector<GameObject*>& getObjects()
	{
		return objects;
	}
	
	/// Creates a dynamic RigidBody with a box as volume.
	RigidBody* createDynamicBox(const Vec2f& position, const Vec2f& extends, float friction = 0.3f);
	
	/// Creates a dynamic RigidBody with a circle as volume.
	RigidBody* createDynamicCircle(const Vec2f& position, float radius, float friction = 0.3f);
	
	/// Creates a static RigidBody with a box as volume.
	RigidBody* createStaticBox(const Vec2f& position, const Vec2f& extends);
	
private:
	/// Since the World is a singleton the consturction is private.
	World();

	void updatePhysics(float delta);
	
	static World* instance;
	
	NavigationGraph* navigationGraph;
	std::vector<GameObject*> objects;
    std::vector<GameObject*> deleteList;
	
	b2World* physicsWorld;	
	PhysicsDebugDraw* physicsDebugDraw;
	
	ContactListener* contactListener;
	
	float remainingTime;
};

#endif


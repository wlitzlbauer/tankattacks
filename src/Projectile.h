
#ifndef __PROJECTILE_H__
#define __PROJECTILE_H__

#include "GameObject.h"

/// Represents a Projectile fired by the Tank in the game world.
class Projectile:
    public GameObject
{
public:
    Projectile(const Vec2f& position, const Vec2f& direction);
    
    virtual void update(float delta);
    
    virtual void drawElement() const;
    
    virtual void onCollision(GameObject* collObjects);
	
private:
	// Duration how long the projectile stays alive.
	static const float MAX_TIMEOUT;
	static const float DAMAGE;
	static const float RADIUS;
	static const float SPEED;
	
	float timeout;
};

#endif

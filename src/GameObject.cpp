#include "stdafx.h"
#include "GameDefines.h"

#include "GameObject.h"
#include "MathUtil.h"
#include "StringUtil.h"
#include "GameObjectObserver.h"

#include "RigidBody.h"

int GameObject::gameObjectCounter = 0;


GameObject::GameObject():
	position(0.0f, 0.0f),
	angle(0.0f),
	radius(0.0f),
	rigidBody(NULL)
{
	setName(StringUtil::format("obj_%d", gameObjectCounter));
	gameObjectCounter++;
}

GameObject::~GameObject()
{
	delete rigidBody;
}

void GameObject::setPosition(const Vec2f& _position)
{
	position = _position;
	
	if (rigidBody != NULL)
	{
		rigidBody->setPosition(position);
	}
}

void GameObject::setAngle(float _angle)
{
	angle = _angle;
	
	if (rigidBody != NULL)
	{
		rigidBody->setAngle(angle);
	}
}

void GameObject::setRigidBody(RigidBody* body)
{
	rigidBody = body;
	
	if (rigidBody != NULL)
	{
		rigidBody->setUserData(this);
	}
}

Vec2f GameObject::getLinearVelocity() const
{
	if (rigidBody != NULL)
	{
		return rigidBody->getLinearVelocity();
	}
	
	return Vec2f::zero();
}

void GameObject::addObserver(GameObjectObserver* observer)
{
    observers.push_back(observer);
}

void GameObject::update(float delta)
{
	// updates the position and angle if a RigidBody is attached.
	
	if (rigidBody != NULL)
	{
		position = rigidBody->getPosition();
		angle = rigidBody->getAngle();
	}
}

void GameObject::draw() const
{
	gl::pushModelView();
	gl::translate (position);
	gl::rotate(angle * MathUtil::RADIANT_TO_DEGREE);
	drawElement();
	gl::popModelView();
}

void GameObject::drawElement() const
{
}

void GameObject::debugDraw() const
{
	gl::pushModelView();
	gl::translate (position);
	gl::rotate(angle * MathUtil::RADIANT_TO_DEGREE);
	gl::color(255, 255, 255);
	gl::drawSolidCircle(Vec2f::zero(), 5);
	gl::popModelView();
}

void GameObject::onCollision(GameObject* collObject)
{
    for (size_t i = 0; i < observers.size(); i++)
    {
        observers[i]->onCollision(collObject);
    }
}

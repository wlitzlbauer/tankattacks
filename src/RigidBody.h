#ifndef __RIGID_BODY__
#define __RIGID_BODY__

#include "Box2D/Box2D.h"
#include "Box2DUtil.h"

class GameObject;

/// Wrapper class of a physics object of the underlaying physics engine (Box2d).
/// Note: Avoid calling functions of the Box2D directly. Instead expose the function here.
class RigidBody
{
public:
	/// Creates a new wrapper for the given Box2D body.
	RigidBody(b2Body* _body):
		body(_body)
	{}
	
	/// Also destructs the Box2D body.
	~RigidBody();
	
	/// Returns the position of the RigidBody.
	Vec2f getPosition() const
	{
		return Box2DUtil::convert(body->GetPosition());
	}
	
	/// Sets the position of the RigidBody.
	void setPosition(const Vec2f& position)
	{
		body->SetTransform(Box2DUtil::convert(position), body->GetAngle());
	}
	
	/// Returns the angle of the RigidBody.
	float getAngle() const
	{
		return body->GetAngle();
	}
	
	/// Sets the angle of the RigidBody.
	void setAngle(float angle)
	{
		body->SetTransform(body->GetPosition(), body->GetAngle());
	}
	
	/// Returns the linear velocity of the RigidBody.
	Vec2f getLinearVelocity() const
	{
		return Box2DUtil::convert(body->GetLinearVelocity());
	}
	
	/// Sets the linear velocity of the RigidBody.
	void setLinearVelocity(const Vec2f& linearVelocity)
	{
		body->SetLinearVelocity(Box2DUtil::convert(linearVelocity));
	}

	/// Sets the transform (position and angle) of the RigidBody.
	void setTransform(const Vec2f& position, float angle)
	{
		body->SetTransform(Box2DUtil::convert(position), angle);
	}
	
	/// Returns the mass of the RigidBody.
	float getMass() const
	{
		return body->GetMass();
	}
	
	/// adds a linear impulse to center of the RigidBody.
	void applyLinearImpulse(const Vec2f& impulse)
	{
		body->ApplyLinearImpulse(Box2DUtil::convert(impulse), body->GetWorldCenter());
	}
	
	/// Adds a linear impulse to the RigidBody.
	void applyLinearImpulse(const Vec2f& impulse, const Vec2f& point)
	{
		body->ApplyLinearImpulse(Box2DUtil::convert(impulse), Box2DUtil::convert(point));
	}
	
	/// Adds a force to the center of the RigidBody.
	void applyForce(const Vec2f& force)
	{
		body->ApplyForceToCenter(Box2DUtil::convert(force));
	}
	
	/// Adds a force to the RigidBody.
	void applyForce(const Vec2f& force, const Vec2f& point)
	{
		body->ApplyForce(Box2DUtil::convert(force), Box2DUtil::convert(point));
	}

	/// Adds a torque to the RigidBody.
	void applyTorque(float torque)
	{
		body->ApplyTorque(torque);
	}
	
	/// Sets the linear damping factor of the RigidBody.
	void setLinearDamping(float damping)
	{
		body->SetLinearDamping(damping);
	}
	
	/// Sets the angular damping factor of the RigidBody.
	void setAngularDamping(float damping)
	{
		body->SetAngularDamping(damping);
	}
	
	/// Returns the center of the RigidBody in world coordinates.
	Vec2f getWorldCenter() const
	{
		return Box2DUtil::convert(body->GetWorldCenter());
	}
	
	/// Converts a point in local space to world space.
	Vec2f getWorldPoint(const Vec2f& localPoint) const
	{
		return Box2DUtil::convert(body->GetWorldPoint(Box2DUtil::convert(localPoint)));
	}
	
	/// Converts a vector in local space to world space.
	Vec2f getWorldVector(const Vec2f& localVector) const
	{
		return Box2DUtil::convert(body->GetWorldVector(Box2DUtil::convert(localVector)));
	}
	
	/// Converts a point in world space to local space.
	Vec2f getLocalPoint(const Vec2f& worldPoint) const
	{
		return Box2DUtil::convert(body->GetLocalPoint(Box2DUtil::convert(worldPoint)));
	}
	
	/// Converst a vector in world space to local space.
	Vec2f getLocalVector(const Vec2f& worldVector) const
	{
		return Box2DUtil::convert(body->GetLocalVector(Box2DUtil::convert(worldVector)));
	}
	
	/// Sets the user data associated with the RigidBody.
	/// Note: Should be a GameObject.
	void setUserData(GameObject* data)
	{
		body->SetUserData(data);
	}

	/// Returns the Box2D body wrapped by this class.
	b2Body* getB2Body()
	{
		return body;
	}
	
private:
	b2Body* body;
};

#endif

#include "stdafx.h"
#include "GameDefines.h"

#include "Projectile.h"

#include "Tank.h"
#include "RigidBody.h"
#include "World.h"

const float Projectile::MAX_TIMEOUT = 5.0f;
const float Projectile::DAMAGE = 0.1f;
const float Projectile::RADIUS = 2.0f;
const float Projectile::SPEED = 100.0f;

Projectile::Projectile(const Vec2f& position, const Vec2f& direction):
    GameObject(),
	timeout(0.0f)
{
    setRadius(RADIUS);
	setPosition(position);

	RigidBody* rigidBody = World::getInstance()->createDynamicCircle(position, RADIUS, 0.0f);
	rigidBody->setLinearVelocity(direction * SPEED);
	setRigidBody(rigidBody);
}

void Projectile::update(float delta)
{
	GameObject::update(delta);
	
    timeout += delta;
    
    if (timeout > MAX_TIMEOUT)
    {
        // out of the world: kill me
        World::getInstance()->addForDelete(this);
    }
}

void Projectile::drawElement() const
{
    gl::color(1.0f, 0.0f, 0.0f);
    gl::drawSolidCircle(Vec2f(0.0f, 0.0f), getRadius());
    gl::color(1.0f, 1.0f, 1.0f);
}

void Projectile::onCollision(GameObject* collObject)
{
    GameObject::onCollision(collObject);
    
	Tank* tank = dynamic_cast<Tank*>(collObject);
	
	// if tank is hit reduce its health
	if (tank != NULL)
	{
		tank->setHealth(tank->getHealth() - DAMAGE);
	}

	World::getInstance()->addForDelete(this);
}
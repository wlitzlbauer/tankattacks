#include "stdafx.h"
#include "GameDefines.h"
#include "WorldInterface.h"
#include "World.h"
#include "Tank.h"
#include "MathUtil.h"


WorldInterface* WorldInterface::instance = NULL;

WorldInterface* WorldInterface::getInstance()
{
	if (instance == NULL)
	{
		instance = new WorldInterface();
	}
	
	return instance;
}

WorldInterface::WorldInterface():
    playerTank(NULL)
{
}

void WorldInterface::getTanksNearby(Tank* myTank, float radius, std::vector<Tank*>& result) const
{
	World* world = World::getInstance();
	const std::vector<GameObject*>& objects = world->getObjects();
	const Vec2f& pos = myTank->getPosition();
	radius += myTank->getRadius();
	
	for (size_t i = 0; i < objects.size(); i++)
	{
		// Note: dynamic cast is slow. In production code this should be optimized.
		Tank* objB = dynamic_cast<Tank*>(objects[i]);
		
		if ((objB != NULL) && (objB != myTank))
		{
			Vec2f distance = pos - objB->getPosition();
			if (distance.lengthSquared() < MathUtil::pow(radius + objB->getRadius()))
			{
				result.push_back(objB);
			}
		}
	}
}

void WorldInterface::getAllTanks(std::vector<Tank*>& result) const
{
	World* world = World::getInstance();
	const std::vector<GameObject*>& objects = world->getObjects();
	
	for (size_t i = 0; i < objects.size(); i++)
	{
		// Note: dynamic cast is slow. In production code this should be optimized.
		Tank* objB = dynamic_cast<Tank*>(objects[i]);
		
		if (objB != NULL)
		{
			result.push_back(objB);
		}
	}
}

Tank* WorldInterface::getTankAt(const Vec2f& position) const
{
	World* world = World::getInstance();
	const std::vector<GameObject*>& objects = world->getObjects();
	
	for (size_t i = 0; i < objects.size(); i++)
	{
		// Note: dynamic cast is slow. In production code this should be optimized.
		Tank* obj = dynamic_cast<Tank*>(objects[i]);
		
		if (obj != NULL)
		{
			Vec2f distance = position - obj->getPosition();
			if (distance.lengthSquared() < MathUtil::pow(obj->getRadius()))
			{
				return obj;
			}
		}
	}
	
	return NULL;
}

float WorldInterface::getDistanceToPlayer(Tank* tank)
{
	if ((tank != NULL) && (playerTank != NULL))
	{
		return tank->getPosition().distance(playerTank->getPosition());
	}
	else
	{
		return -1;
	}
}


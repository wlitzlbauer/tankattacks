#include "stdafx.h"
#include "GameDefines.h"

#include "ScoutTankAI.h"
#include "WorldInterface.h"
#include "Tank.h"
#include "MathUtil.h"
#include "StringUtil.h"

ScoutTankAI::ScoutTankAI(Tank* _tank, WorldInterface* _worldInterface):
	TankAI(_tank, _worldInterface)
{

}

ScoutTankAI::~ScoutTankAI()
{
}

void ScoutTankAI::update(float delta)
{
	Tank::Steering steering;
	
	Tank* tank = getTank();
	Tank* playerTank = getWorldInterface()->getPlayerTank();
	
	steering.linear = seek(playerTank->getPosition());
	steering.angular = lookForward(tank->getLinearVelocity() + steering.linear);
	steering.aiming = 0;
	
	tank->setSteering(steering);
}

void ScoutTankAI::debugDraw()
{
}

/// Returs a steering linear toward the given target.
Vec2f ScoutTankAI::seek(const Vec2f& target)
{
	Tank* tank = getTank();
	
	Vec2f currentVelocity = tank->getLinearVelocity();
	Vec2f desiredVelocity = target - tank->getPosition();
	desiredVelocity.limit(Tank::MAX_SPEED);
	return desiredVelocity - currentVelocity;
}

Vec2f ScoutTankAI::arrive(const Vec2f& target)
{
	// Implementation missing
	return Vec2f::zero();
}


Vec2f ScoutTankAI::pursue(Tank* target)
{
	// Implementation missing
	return Vec2f::zero();
}

float ScoutTankAI::align(float targetOrientation)
{
	const float DECELERATION = 0.5f;
    const float DISTANCE_THRESHOLD = 0.01f;
	
	Tank* tank = getTank();
    float rotation = targetOrientation - tank->getAngle();
    rotation = MathUtil::mapToRange(rotation);
	float rotationSize = fabs(rotation);
    
    if (rotationSize < DISTANCE_THRESHOLD)
    {
        return 0.0f;
    }
    
	rotation /= DECELERATION;
    return MathUtil::clamp(rotation, -Tank::MAX_ROTATION, Tank::MAX_ROTATION);
}

float ScoutTankAI::lookForward(const Vec2f& velocity)
{
	if (velocity.lengthSquared() < MathUtil::ROUNDING_ERROR)
	{
		return 0.0f;
	}
	
	return align(atan2(-velocity.x, velocity.y));
}


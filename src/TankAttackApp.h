#ifndef __TANK_ATTACK_APP_H__
#define __TANK_ATTACK_APP_H__

class World;
class WorldInterface;
class GameObject;
class HumanController;
class TankAI;
class Tank;

/// The base application class
/// Initializes and updates the game loop.
class TankAttackApp: 
	public app::AppBasic 
{
public:
	TankAttackApp();
	void prepareSettings(Settings *settings);
	void setup();
	void mouseDown(MouseEvent event);	
	void keyDown(KeyEvent event);
	void keyUp(KeyEvent event);
	void update();
	void draw();

private:	
	World* world;
	WorldInterface* worldInterface;
	HumanController* player;
	TankAI* tankAI;
	std::vector<Tank*> tanks;
	
	bool showDebugDraw;
    bool showNavigationMesh;
	bool showPhysics;
	double lastTime;
};

#endif

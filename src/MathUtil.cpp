#include "stdafx.h"
#include "GameDefines.h"

#include "MathUtil.h"
#include <stdio.h>
#include <stdlib.h>

const float MathUtil::PI = 3.141593f;
const float MathUtil::PI_2 = 6.283185f;
const float MathUtil::PI_HALF = 1.570796f;
const float MathUtil::PI_QUARTER = 0.785398f;

const float MathUtil::RADIANT_TO_DEGREE = 57.295780f;
const float MathUtil::DEGREE_TO_RADIANT = 0.017453f;

const float MathUtil::ROUNDING_ERROR = 0.0001f;


int MathUtil::random(int max)
{
	return rand() % max;
}

float MathUtil::randomBinomial()
{
	return ((float) rand() / RAND_MAX) - ((float) rand() / RAND_MAX);
}

float MathUtil::mapToRange(float angle)
{
	while (angle > PI)
	{
		angle -= PI_2;
	}
	while (angle < -PI)
	{
		angle += PI_2;
	}
	
	return angle;
}

#ifndef __ACTION_H__
#define __ACTION_H__


class TankAI;
class Tank;

class Action
{
public:
    Action(TankAI* _ai);
	
	virtual ~Action() {};
    
    /// Called when Action get active
    virtual void onActivated();
    
    /// Called every frame while Action is active
    virtual void update(float delta);
	
	/// Returns true if the Action is completed
	virtual bool isComplete()
	{
		return true;
	}
    
    virtual void debugDraw()
    {}
	
	/// Debug string of this class
	virtual const char* getDebugName()
	{
		return "Action";
	}
    
    /// Returns the TankAI
    TankAI* getAI()
    {
        return ai;
    }
    
    /// Returns the assoziated Tank GameObject
    Tank* getTank()
    {
        return tank;
    }
    
private:
    TankAI* ai;
    Tank* tank;
};

#endif

#ifndef __CONNECTION_H__
#define __CONNECTION_H__

class Node;

class Connection
{
public:
	Connection()
	{}

	Connection(Node* _fromNode, Node* _toNode, float _cost):
		fromNode(_fromNode),
		toNode(_toNode),
		cost(_cost)
	{}
	
	Node* getFromNode() const
	{
		return fromNode;
	}
	
	Node* getToNode() const
	{
		return toNode;
	}
	
	float getCost() const
	{
		return cost;
	}
	
private:
	Node* fromNode;
	Node* toNode;
	float cost;
};

#endif
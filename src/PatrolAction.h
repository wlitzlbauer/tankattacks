
#ifndef __PATROL_ACTION_H__
#define __PATROL_ACTION_H__

#include "Action.h"

class PatrolAction:
    public Action
{
public:
    PatrolAction(TankAI* _ai);
    
    virtual void onActivated();
    
    virtual void update(float delta);
    
    virtual void debugDraw();
	
	virtual const char* getDebugName()
	{
		return "PatrolAction";
	}
    
private:
    int getClosestWaypoint(const Vec2f& position);
    
    std::vector<Vec2f> waypoints;
    int currentIdx;
};

#endif

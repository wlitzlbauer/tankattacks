#include "stdafx.h"
#include "GameDefines.h"
#include "Action.h"
#include "TankAI.h"

Action::Action(TankAI* _ai):
    ai(_ai)
{
    tank = ai->getTank();
}

void Action::onActivated()
{
    
}

void Action::update(float delta)
{
    
}
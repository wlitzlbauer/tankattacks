
#include "stdafx.h"
#include "GameDefines.h"

#include "SimpleTankAI.h"
#include "WorldInterface.h"
#include "Tank.h"
#include "MathUtil.h"
#include "StringUtil.h"
#include "DrawingUtil.h"
#include "PursueAction.h"
#include "PatrolAction.h"
#include "FleeAction.h"
#include "FindPathAction.h"

SimpleTankAI::SimpleTankAI(Tank* _tank, WorldInterface* _worldInterface):
	TankAI(_tank, _worldInterface),
	currentAction(NULL),
	pursueAction(NULL)
{
    pursueAction = new PursueAction(this);
    pursueAction->setEvader(getWorldInterface()->getPlayerTank());
    
    patrolAction = new PatrolAction(this);
    
    fleeAction = new FleeAction(this);
    fleeAction->setPursuer(getWorldInterface()->getPlayerTank());
    
    //changeAction(patrolAction);
	
	// temp for A*
	FindPathAction* findPathAction = new FindPathAction(this);
	findPathAction->setTarget(getWorldInterface()->getPlayerTank());
	
	changeAction(findPathAction);
}

SimpleTankAI::~SimpleTankAI()
{
    delete pursueAction;
    delete patrolAction;
    delete fleeAction;
}

void SimpleTankAI::changeAction(Action* newAction)
{
    if (currentAction != newAction)
    {
        newAction->onActivated();
        currentAction = newAction;
    }
}

void SimpleTankAI::update(float delta)
{	
    /// TODO implement Decision making
    currentAction->update(delta);
}

void SimpleTankAI::debugDraw()
{
	if (currentAction != NULL)
	{
		currentAction->debugDraw();
		DrawingUtil::drawText(getTank()->getPosition() + Vec2f(0, 15), currentAction->getDebugName());
	}
}
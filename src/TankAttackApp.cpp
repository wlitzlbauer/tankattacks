#include "stdafx.h"
#include "GameDefines.h"

#include "TankAttackApp.h"

#include "World.h"
#include "WorldInterface.h"
#include "NavigationGraph.h"
#include "GameObject.h"
#include "Tank.h"
#include "TacticalPathfindingTankAI.h"
#include "Wall.h"
#include "HumanController.h"
#include "TankAI.h"
#include "StringUtil.h"
#include "DrawingUtil.h"
#include "Node.h"

/*#include  "UnitTest++.h"

TEST(FailSpectacularly)
{
    CHECK(false);
}*/


TankAttackApp::TankAttackApp():
	world(NULL),
	player(NULL),
	tankAI(NULL),
	showDebugDraw(false),
    showNavigationMesh(false),
	showPhysics(false)
{
	//printf("tests: %d", UnitTest::RunAllTests());
}

void TankAttackApp::prepareSettings(Settings *settings)
{
    settings->setWindowSize(World::WORLD_WIDTH, World::WORLD_HEIGHT);
    settings->setFrameRate(60.0f);
}

void TankAttackApp::setup()
{
	DrawingUtil::init();
	
	// 1) create the world
	world = World::getInstance();
	
	// 2) create the world interface used by the AI
	worldInterface = WorldInterface::getInstance();
	
	// 3) populate the world with obstacles
	world->addObject(new Wall(Vec2f(0.0f, 300.0f), Vec2f(100.0f, 300.0f), 3.0f));
	world->addObject(new Wall(Vec2f(100.0f, 100.0f), Vec2f(100.0f, 300.0f), 3.0f));
	world->addObject(new Wall(Vec2f(200.0f, 100.0f), Vec2f(200.0f, 250.0f), 3.0f));
	world->addObject(new Wall(Vec2f(200.0f, 100.0f), Vec2f(500.0f, 100.0f), 3.0f));
    
    world->addObject(new Wall(Vec2f(500.0f, 300.0f), Vec2f(500.0f, 700.0f), 3.0f));
    world->addObject(new Wall(Vec2f(600.0f, 100.0f), Vec2f(600.0f, 500.0f), 3.0f));
    world->addObject(new Wall(Vec2f(600.0f, 100.0f), Vec2f(700.0f, 100.0f), 3.0f));
    
    world->addObject(new Wall(Vec2f(600.0f, 500.0f), Vec2f(700.0f, 500.0f), 3.0f));

	// border arround the world
	world->addObject(new Wall(Vec2f(0.0f, 0.0f), Vec2f((float) World::WORLD_WIDTH, 0.0f), 3.0f));
	world->addObject(new Wall(Vec2f((float) World::WORLD_WIDTH, 0.0f), Vec2f((float) World::WORLD_WIDTH, (float) World::WORLD_HEIGHT), 3.0f));
	world->addObject(new Wall(Vec2f((float) World::WORLD_WIDTH, (float) World::WORLD_HEIGHT), Vec2f(0.0f, (float) World::WORLD_HEIGHT), 3.0f));
	world->addObject(new Wall(Vec2f(0.0f, (float) World::WORLD_HEIGHT), Vec2f(0.0f, 0.0f), 3.0f));
	
	// 4) calculate navigation mesh
	world->getNavigationGraph()->initGraph();
	
	// 5) create Tanks
	Tank* playerTank = new Tank(Tank::FACTION_BLUE);
	playerTank->setPosition(Vec2f(425.0f, 425.0f));
	world->addObject(playerTank);
    worldInterface->setPlayerTank(playerTank);
	
	playerTank = new Tank(Tank::FACTION_BLUE);
	playerTank->setPosition(Vec2f(325.0f, 325.0f));
	world->addObject(playerTank);
	
	playerTank = new Tank(Tank::FACTION_BLUE);
	playerTank->setPosition(Vec2f(425.0f, 325.0f));
	world->addObject(playerTank);
	
	Tank* agentTank1 = new Tank(Tank::FACTION_RED);
	agentTank1->setPosition(Vec2f(525.0f, 225.0f));
	world->addObject(agentTank1);
	

	// 6) create HumanController and AI
	player = new HumanController(playerTank);
	tankAI = new TacticalPathfindingTankAI(agentTank1, worldInterface);
	
	// 7) query all tanks
	worldInterface->getAllTanks(tanks);
	
	// world creation done
	
	lastTime = cinder::app::getElapsedSeconds();
}

void TankAttackApp::mouseDown(MouseEvent event)
{	
	tankAI->mouseDown(event.getX(), event.getY(), event.isAltDown());
}

void TankAttackApp::keyDown(KeyEvent event)
{
	if (player)
	{
		player->keyDown(event);
	}
}

void TankAttackApp::keyUp(KeyEvent event)
{
	if (player)
	{
		player->keyUp(event);
	}
	
	switch (event.getCode())
	{
		case '1':
			showDebugDraw = !showDebugDraw;
			break;
        case '2':
            showNavigationMesh = !showNavigationMesh;
            break;
		case '3':
			showPhysics = !showPhysics;
			break;
	}
}

void TankAttackApp::update()
{
	double currentTime = cinder::app::getElapsedSeconds();
	float delta = (float) (currentTime - lastTime);

	// 1) Update HumanControlloer and Agent
	player->update(delta);
	tankAI->update(delta);
	
	// 2) Now update the world
	world->update(delta);
	
	// 3) Update influence map
	world->getNavigationGraph()->updateInfluenceMap(tanks);
	
	lastTime = currentTime;
}

void TankAttackApp::draw()
{
	gl::pushModelView();
	
	// clear out the window with black
	gl::clear( Color( 0, 0, 0 ) ); 
	gl::enableAlphaTest();
	gl::enableAlphaBlending();
	gl::color(255, 255, 255);
	
    if (showNavigationMesh)
    {
        world->getNavigationGraph()->debugDraw();
    }
	
	world->draw();
    
	if (showPhysics)
	{
		world->debugDrawPhysics();
	}
	
	if (showDebugDraw)
	{
		world->debugDraw();
	}
	
	
	if (showDebugDraw)
	{
		tankAI->debugDraw();
	}
	
	gl::popModelView();
	
	DrawingUtil::drawText(Vec2f(10.0f, 14.0f), "fps %.2f", getAverageFps());
}


CINDER_APP_BASIC( TankAttackApp, RendererGl )

#ifndef __BEHAVIOR_TREE_TANK_AI__
#define __BEHAVIOR_TREE_TANK_AI__

#include "TankAI.h"

class Tank;
class WorldInterface;
class Task;

/// AI using a behavior tree
class BehaviorTreeTankAI:
	public TankAI
{
public:
	BehaviorTreeTankAI(Tank* _tank, WorldInterface* _worldInterface);
    
    virtual ~BehaviorTreeTankAI();
	
	virtual void update(float delta);
	
private:
	Task* behaviorTree;
};

#endif

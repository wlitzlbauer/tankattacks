#include "stdafx.h"
#include "GameDefines.h"
#include "PhysicsDebugDraw.h"
#include "Box2DUtil.h"

/////////////////////////////////////////////////////////////////////////////////////////////////
// Disclaimer:
// Implementation was copy pasted from various internet sources or quickly hacked.
// Don't reuse this code in other projects!
/////////////////////////////////////////////////////////////////////////////////////////////////


void PhysicsDebugDraw::DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color)
{
	//set up vertex array
    GLfloat glverts[16]; //allow for polygons up to 8 vertices
    glVertexPointer(2, GL_FLOAT, 0, glverts); //tell OpenGL where to find vertices
    glEnableClientState(GL_VERTEX_ARRAY); //use vertices in subsequent calls to glDrawArrays
    
    //fill in vertex positions as directed by Box2D
    for (int i = 0; i < vertexCount; i++) {
		glverts[i*2]   = vertices[i].x;
		glverts[i*2+1] = vertices[i].y;
    }
    
    //draw solid area
    glColor4f( color.r, color.g, color.b, 1);
    glDrawArrays(GL_TRIANGLE_FAN, 0, vertexCount);
	
    //draw lines
    glLineWidth(3); //fat lines
    glColor4f( 1, 0, 1, 1 ); //purple
    glDrawArrays(GL_LINE_LOOP, 0, vertexCount);
	
	// set back states
	glLineWidth(1);
    glColor4f(1, 1, 1, 1 );
}

void PhysicsDebugDraw::DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color)
{
	//set up vertex array
    GLfloat glverts[16]; //allow for polygons up to 8 vertices
    glVertexPointer(2, GL_FLOAT, 0, glverts); //tell OpenGL where to find vertices
    glEnableClientState(GL_VERTEX_ARRAY); //use vertices in subsequent calls to glDrawArrays
    
    //fill in vertex positions as directed by Box2D
    for (int i = 0; i < vertexCount; i++) {
		glverts[i*2]   = vertices[i].x;
		glverts[i*2+1] = vertices[i].y;
    }
    
    //draw solid area
    glColor4f( color.r, color.g, color.b, 1);
    glDrawArrays(GL_TRIANGLE_FAN, 0, vertexCount);
	
    //draw lines
    glLineWidth(3); //fat lines
    glColor4f( 1, 0, 1, 1 ); //purple
    glDrawArrays(GL_LINE_LOOP, 0, vertexCount);
	
	// set back states
	glLineWidth(1);
    glColor4f(1, 1, 1, 1 );
}

void PhysicsDebugDraw::DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color)
{
	gl::color(color.r, color.g, color.b);
	gl::drawSolidCircle(Box2DUtil::convert(center), radius);
}
void PhysicsDebugDraw::DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color)
{
	gl::color(color.r, color.g, color.b);
	gl::drawSolidCircle(Box2DUtil::convert(center), radius);
}

void PhysicsDebugDraw::DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color) {}
void PhysicsDebugDraw::DrawTransform(const b2Transform& xf) {}

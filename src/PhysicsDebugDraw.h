#ifndef __PHYSICS_DEBUG_DRAW__
#define __PHYSICS_DEBUG_DRAW__

#include "Box2D/Box2D.h"

/// Debug drawing utils for Box2D.
/// Note: implementation was copy pasted from various internet sources or quickly hacked.
///       Don't reuse this code in other projects!
class PhysicsDebugDraw : public b2Draw
{
public:
    void DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color);
    void DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color);
    void DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color);
    void DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color);
    void DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color);
    void DrawTransform(const b2Transform& xf);
};

#endif

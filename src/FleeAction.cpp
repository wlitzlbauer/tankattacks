#include "stdafx.h"
#include "GameDefines.h"
#include "FleeAction.h"
#include "TankAI.h"
#include "Tank.h"

FleeAction::FleeAction(TankAI* _ai):
    Action(_ai),
    pursuer(NULL)
{
}

void FleeAction::update(float delta)
{
    TankAI* ai = getAI();
	Tank* tank = getTank();
	
	Tank::Steering steering;
	
	if (pursuer != NULL)
	{
		steering.linear = ai->evade(pursuer);
	}
	steering.angular = ai->lookForward(tank->getLinearVelocity() + steering.linear);
	steering.aiming = ai->aimForward(tank->getLinearVelocity() + steering.linear);
	
	tank->setSteering(steering);
}

#ifndef __TANK_H__
#define __TANK_H__

#include "GameObject.h"
#include "Mathutil.h"

#include "cinder/gl/Texture.h"

#include "Box2D/Box2D.h"

/// Represents a tank in the game.
class Tank: 
	public GameObject
{	
public:
	/// The steering command of the Tank.
	struct Steering
	{
		/// linear acceleration
		Vec2f linear;
		
		/// angular acceleration
		float angular;
		
		/// angular acceleration for aiming
		float aiming;
		
		Steering();
	};
	
	enum Faction
	{
		FACTION_RED,
		FACTION_BLUE
	};
	
	/// Constructs a new Tank
	/// Type: Defines the type of the Tank.
	Tank(Faction _faction);
	
	/// Sets the steering command for the Tank.
	void setSteering(const Steering& _steering)
	{
		steering = _steering;
	}

	/// Returns the current speed of the Tank.
	float getSpeed() const
	{
		return getLinearVelocity().length();
	}
    
    float getHealth() const
    {
        return health;
    }
    
    void setHealth(float _health)
    {
        health = MathUtil::clamp(_health, 0.0f, 1.0f);
    }
	
	float getAimingAngle() const
	{
		return aimingAngle;
	}

    void fire();
	
	virtual void update(float delta); 
	
	virtual void draw() const;
	
	virtual void debugDraw() const;
	
	/// Returns the faction the tank is belonging to.
	Faction getFaction() const
	{
		return faction;
	}
	
	/// The maximum speed of the Tank.
	const static float MAX_SPEED;
	
	/// the maximum rotation speed of the Tank.
	const static float MAX_ROTATION;
    
    /// time needed for reloading
    const static float RELOAD_TIME;
	
	/// time needed for regeneration
	const static float HEALTH_REGENERATION;
	
private:
	/// The radius of the Tank.
	/// Note: use getRadius() if you want to access it from outside.
	const static float RADIUS;
    
    void updateMovement(float delta);
	
	Vec2f getLateralVelocity() const;
	
	/// The texture used to render the Tank.
	gl::Texture bodyTexture;
	gl::Texture gunTexture;
	
	/// The steering command of the Tank.
	Steering steering;
    
	/// angle of the gun.
	float aimingAngle;
	
    /// The current health of the Tank
    float health;
    
    float reloadTimer;
	
	Faction faction;
};

#endif
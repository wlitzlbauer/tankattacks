#ifndef __SCOUT_TANK_AGENT_H__
#define __SCOUT_TANK_AGENT_H__

#include "TankAI.h"

class Tank;
class WorldInterface;

class Action;
class PursueAction;
class PatrolAction;
class FleeAction;

/// Simple Action based character AI.
/// Only 1 Action can be active at a time.
class SimpleTankAI:
	public TankAI
{
public:
	SimpleTankAI(Tank* _tank, WorldInterface* _worldInterface);
    
    virtual ~SimpleTankAI();
	
	virtual void update(float delta);
	
	virtual void debugDraw();
    
protected:
    /// Change the current action
    void changeAction(Action* newAction);
	
private:
    Action* currentAction;
    PursueAction* pursueAction;
    PatrolAction* patrolAction;
    FleeAction* fleeAction;
};

#endif
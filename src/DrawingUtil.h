#ifndef __DRAWING_UTIL__
#define __DRAWING_UTIL__

#include "cinder/gl/TextureFont.h"

#include <string>

/// Utility class for rendering debug text.
class DrawingUtil
{
public:
	/// initializes the Drawing utilities.
	static void init();
	
	/// draws a text on the given position using var arguments.
    static void drawText(const Vec2f& position, const char* format, ...);
	
	/// draws a text on the given position using a string.
	static void drawText(const Vec2f& position, const std::string& text);
	
private:
	static gl::TextureFontRef textureFont;
};

#endif

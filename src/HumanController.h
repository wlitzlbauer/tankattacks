#ifndef __HUMAN_CONTROLLER_H__
#define __HUMAN_CONTROLLER_H__


class Tank;

/// Human controller
/// Translates human input to steering command.
class HumanController
{
public:
	HumanController(Tank* _tank);
	
	void keyDown(KeyEvent event);
	void keyUp(KeyEvent event);
	
	void update(float delta);
	
private:
	Tank* tank;
	
	int move;
	int steer;
    int fire;
	int aiming;
	
	float aimingOffset;
};

#endif
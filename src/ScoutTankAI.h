#ifndef __SCOUT_TANK_AGENT_H__
#define __SCOUT_TANK_AGENT_H__

#include "TankAI.h"

class Tank;
class WorldInterface;


class ScoutTankAI:
	public TankAI
{
public:
	ScoutTankAI(Tank* _tank, WorldInterface* _worldInterface);
    
    virtual ~ScoutTankAI();
	
	virtual void update(float delta);
	
	virtual void debugDraw();
    
protected:

private:
	/// Returs a steering linear toward the given target.
    Vec2f seek(const Vec2f& target);
    
    /// Returns a steering linear toward the given target.
    /// When close to target the steering linear will break.
    Vec2f arrive(const Vec2f& target);
    
    /// Returns a steering linear toward the future position of the target.
    Vec2f pursue(Tank* target);
	
	/// Returns a steering angular to rotate to the given orientation.
	float align(float targetOrientation);
	
	/// Returns a steering angular to look to the given velocity.
	float lookForward(const Vec2f& velocity);
};

#endif
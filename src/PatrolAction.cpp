#include "stdafx.h"
#include "GameDefines.h"
#include "PatrolAction.h"

#include "TankAi.h"
#include "Tank.h"

PatrolAction::PatrolAction(TankAI* _ai):
    Action(_ai),
    currentIdx(0)
{
    waypoints.push_back(Vec2f(150.0f, 50.0f));
    waypoints.push_back(Vec2f(550.0f, 50.0f));
    waypoints.push_back(Vec2f(750.0f, 50.0f));
    waypoints.push_back(Vec2f(750.0f, 300.0f));
    waypoints.push_back(Vec2f(750.0f, 550.0f));
    waypoints.push_back(Vec2f(550.0f, 550.0f));
    waypoints.push_back(Vec2f(550.0f, 300.0f));
    waypoints.push_back(Vec2f(450.0f, 200.0f));
    waypoints.push_back(Vec2f(250.0f, 500.0f));
    waypoints.push_back(Vec2f(150.0f, 400.0f));

}

void PatrolAction::onActivated()
{
    currentIdx = getClosestWaypoint(getAI()->getTank()->getPosition());   
}

void PatrolAction::update(float delta)
{
    const float PATROL_SPEED = 30.0f;
    
    TankAI* ai = getAI();
    Tank* tank = ai->getTank();
    
    Vec2f currentWaypoint = waypoints[currentIdx];
    Vec2f distanceToWaypoint = currentWaypoint - tank->getPosition();
    
    if (distanceToWaypoint.length() < 50.0f)
    {
        currentIdx = (currentIdx + 1) % waypoints.size();
        currentWaypoint = waypoints[currentIdx];
    }
    
    Vec2f linear = ai->seek(currentWaypoint);
    linear.limit(PATROL_SPEED);
	
	Tank::Steering steering;
	
	steering.linear = linear;
	steering.angular = ai->lookForward(tank->getLinearVelocity() + steering.linear);
	steering.aiming = ai->aimForward(tank->getLinearVelocity() + steering.linear);
	
	tank->setSteering(steering);
}

int PatrolAction::getClosestWaypoint(const Vec2f& position)
{
    float closestDistance = 1000000.0f;
    int result = 0;
    
    for (size_t i = 0; i < waypoints.size(); i++)
    {
        Vec2f distance = waypoints[i] - position;
        float d = distance.lengthSquared();
        
        if (d < closestDistance)
        {
            closestDistance = d;
            result = i;
        }
    }
    
    return result;
}

void PatrolAction::debugDraw()
{
    if (waypoints.size() > 0)
    {
        Vec2f lastPoint = waypoints.back();
        
        for (size_t i = 0; i < waypoints.size(); i++)
        {
            Vec2f currentPoint = waypoints[i];
            gl::drawLine(lastPoint, currentPoint);
			
			if (i == currentIdx)
			{
				gl::color(255, 0, 0);
			}
            gl::drawStrokedCircle(currentPoint, 3.0f);
            lastPoint = currentPoint;
			gl::color(255, 255, 255);
        }
    }
}
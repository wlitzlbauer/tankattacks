#include "stdafx.h"
#include "GameDefines.h"

#include "Tank.h"

#include "cinder/app/AppBasic.h"
#include "cinder/ImageIo.h"

#include "World.h"
#include "MathUtil.h"
#include "DrawingUtil.h"
#include "StringUtil.h"
#include "RigidBody.h"

#include "Projectile.h"

#include "TankAttackApp.h"

const float Tank::RADIUS = 20.0f;
const float Tank::MAX_SPEED = 50.0f;
const float Tank::MAX_ROTATION = MathUtil::PI_QUARTER;
const float Tank::HEALTH_REGENERATION = 0.01f;  // health added per second
const float Tank::RELOAD_TIME = 1.0f;


Tank::Steering::Steering():
	linear(0.0f, 0.0f),
	angular(0.0f),
	aiming(0.0f)
{
}

Tank::Tank(Faction _faction):
	GameObject(),
    health(0.5f),
	aimingAngle(0.0f),
	faction(_faction)
{
	setRadius(RADIUS);
	
	RigidBody* body = World::getInstance()->createDynamicBox(getPosition(), Vec2f(RADIUS/2, RADIUS));
	body->setAngularDamping(3.0f);
	body->setLinearDamping(1.0f);
	setRigidBody(body);
	
	switch (faction)
	{
		case FACTION_RED:
			bodyTexture = gl::Texture(loadImage(loadAsset("playerBody.png")));
			gunTexture = gl::Texture(loadImage(loadAsset("playerGun.png")));
			break;
		case FACTION_BLUE:
			bodyTexture = gl::Texture(loadImage(loadAsset("enemyBody.png")));
			gunTexture = gl::Texture(loadImage(loadAsset("enemyGun.png")));
			break;
	}
}

void Tank::fire()
{
    if (reloadTimer > 0.0f)
    {
        return;
    }
    
	Vec2f direction(0.0f, 1.0f);
	direction.rotate(aimingAngle);
	
	Vec2f position = getPosition() + direction * (getRadius() + 15.0f);
	
    // Note: Creating a new object with "new" during the game is a bad idea
    // (-> memory fragmentation, out of memory issues, memory leaks)
    // Better to reuse old Projectiles and use a object pool.
    Projectile* projectile = new Projectile(position, direction);
    
    World::getInstance()->addObject(projectile);
    
    reloadTimer = RELOAD_TIME;
}


Vec2f Tank::getLateralVelocity() const
{
	Vec2f currentRightNormal = getRigidBody()->getWorldVector(Vec2f(1,0));
	return currentRightNormal.dot(getRigidBody()->getLinearVelocity()) * currentRightNormal;
}


void Tank::updateMovement(float delta)
{
	const float FRICTION_STRENGHT = 0.25f;
	const float STEERING_LINEAR_STRENGTH = 100.0f;
	const float STEERING_ANGULAR_STRENGTH = 700.0f;
	
	RigidBody* body = getRigidBody();

	// apply wheel friction
	Vec2f impulse = body->getMass() * -getLateralVelocity() * FRICTION_STRENGHT;
	body->applyLinearImpulse(impulse);

	// apply steering command
	Vec2f steeringForce = body->getMass() * steering.linear * STEERING_LINEAR_STRENGTH;
	steeringForce.limit(body->getMass() * MAX_SPEED);
	body->applyForce(steeringForce);
	body->applyTorque(body->getMass() * steering.angular * STEERING_ANGULAR_STRENGTH);
}

void Tank::update(float delta)
{
	GameObject::update(delta);
    
    // update health
    health = MathUtil::clamp(health + HEALTH_REGENERATION * delta, 0.0f, 1.0f);
    
    // update reloading
    reloadTimer = MathUtil::clamp(reloadTimer - delta, 0.0f, RELOAD_TIME);
    
	// update movement
	updateMovement(delta);
	
	// update aiming
	aimingAngle += steering.aiming * delta * 2;
}


void Tank::draw() const
{
    gl::pushModelView();

	gl::translate (getPosition());
	
    gl::pushModelView();
	gl::rotate(getAngle() * MathUtil::RADIANT_TO_DEGREE);
	gl::draw(bodyTexture, Rectf(Vec2f(-20.0f, -15.0f), Vec2f(20.0f, 25.0f)));
    gl::popModelView();
	
	
	gl::pushModelView();
	gl::rotate(aimingAngle * MathUtil::RADIANT_TO_DEGREE);
	gl::draw(gunTexture, Rectf(Vec2f(-20.0f, -15.0f), Vec2f(20.0f, 25.0f)));
	gl::popModelView();
    
    // DRAW HEALTH
    const Vec2f BOX_SIZE(15.0f, 5.0f);
    const float BOX_OFFSET = 15.0f;
    
    Rectf background(-BOX_SIZE.x, BOX_OFFSET, BOX_SIZE.x, BOX_OFFSET + BOX_SIZE.y);
    gl::color(1.0f, 1.0f, 1.0f);
    gl::drawSolidRect(background);
    Rectf forground(-BOX_SIZE.x + 1.0f, BOX_OFFSET + 1, 
                    -BOX_SIZE.x + 1.0f + (BOX_SIZE.x * 2.0f - 2.0f) * health, BOX_OFFSET + BOX_SIZE.y - 1.0f);
    gl::color(0.0f, 0.75f, 0.0f);
    gl::drawSolidRect(forground);
	
	// set back color
    gl::color(1.0f, 1.0f, 1.0f);
    gl::popModelView();
}

void Tank::debugDraw() const
{
	GameObject::debugDraw();
	
	gl::pushModelView();
	
	gl::translate (getPosition());
	gl::color(1.0f, 0.0f, 0.0f);
    gl::drawLine(getDirection() * 20.0f, getDirection() * (MAX_SPEED + 20.0f));

	gl::color(0.0f, 1.0f, 0.0f);
    gl::drawLine(getDirection() * 20.0f, getDirection() * (getSpeed() + 20.0f));
    
	// set back color
	gl::color(1.0f, 1.0f, 1.0f);
	gl::popModelView();
	
	DrawingUtil::drawText(getPosition(), "vel: %.2f", getSpeed());
}
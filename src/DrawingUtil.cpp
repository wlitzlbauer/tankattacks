#include "stdafx.h"
#include "GameDefines.h"

#include "DrawingUtil.h"
#include "StringUtil.h"

gl::TextureFontRef DrawingUtil::textureFont = NULL;

void DrawingUtil::init()
{
	Font font = Font("Consolas", 14);
	textureFont = gl::TextureFont::create(font);
}

void DrawingUtil::drawText(const Vec2f& position, const char* format, ...)
{
	// Copy paste from StringUtil put very handy
	
	char buffer[StringUtil::BUFFER_SIZE];
	va_list args;
	va_start(args, format);
	
#ifdef _WIN32
	vsnprintf_s(buffer, sizeof(buffer), StringUtil::BUFFER_SIZE, format, args);
#else
	vsnprintf(buffer, StringUtil::BUFFER_SIZE, format, args);
#endif
    
	va_end(args);
	return drawText(position, std::string(buffer));
}

void DrawingUtil::drawText(const Vec2f& position, const std::string& text)
{
	gl::color(0, 0, 0);
	textureFont->drawString(text, position + Vec2f(1, 1));
	
	gl::color(1, 1, 1);
	textureFont->drawString(text, position);
}




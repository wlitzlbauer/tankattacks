#include "stdafx.h"
#include "GameDefines.h"

#include "TankAI.h"
#include "Tank.h"
#include "MathUtil.h"
#include "WorldInterface.h"

TankAI::TankAI(Tank* _tank, WorldInterface* _worldInterface):
	tank(_tank),
	worldInterface(_worldInterface)
{
    tank->addObserver(this);
}

TankAI::~TankAI()
{
    
}

void TankAI::mouseDown(int x, int y, bool altPressed)
{
}


void TankAI::update(float delta)
{
}

void TankAI::debugDraw()
{
}

/// Returs a steering linear toward the given target.
Vec2f TankAI::seek(const Vec2f& target)
{
	Vec2f currentVelocity = tank->getLinearVelocity();
	Vec2f desiredVelocity = target - tank->getPosition();
	desiredVelocity.limit(Tank::MAX_SPEED);
	return desiredVelocity; // - currentVelocity;
}

Vec2f TankAI::arrive(const Vec2f& target)
{
	const float DECELERATION = 3.0f;
    const float DISTANCE_THRESHOLD = 1.0f;
	
    Vec2f distance = target - tank->getPosition();
    float distanceLen = distance.length();
    
    if (distanceLen < DISTANCE_THRESHOLD)
    {
        // stop the tank
        return tank->getLinearVelocity() * -1.0f;
    }
    
    float speed = distanceLen / DECELERATION;
    speed = MathUtil::clamp(speed, 0.0f, Tank::MAX_SPEED);
    Vec2f desiredVelocity = distance;
    desiredVelocity.limit(speed);
    return desiredVelocity - tank->getLinearVelocity();
}


Vec2f TankAI::pursue(Tank* target)
{
	Vec2f toEvader = target->getPosition() - getTank()->getPosition();
	float distanceToEvader = toEvader.length();
	
	// calculate lookahead time based on distance and speeds
	float lookAheadTime = distanceToEvader / (Tank::MAX_SPEED + target->getSpeed());
	return seek(target->getPosition() + (target->getLinearVelocity() * lookAheadTime));
}


Vec2f TankAI::flee(const Vec2f& target)
{
	Vec2f currentVelocity = tank->getLinearVelocity();
	Vec2f desiredVelocity = tank->getPosition() - target;
	desiredVelocity.limit(Tank::MAX_SPEED);
	return desiredVelocity - currentVelocity;
}


Vec2f TankAI::evade(Tank* target)
{
	Vec2f toEvader = target->getPosition() - getTank()->getPosition();
	float distanceToEvader = toEvader.length();
	
	// calculate lookahead time based on distance and speeds
	float lookAheadTime = distanceToEvader / (Tank::MAX_SPEED + target->getSpeed());
	return flee(target->getPosition() + (target->getLinearVelocity() * lookAheadTime));
}

float TankAI::align(float targetOrientation)
{
	const float DECELERATION = 0.5f;
    const float DISTANCE_THRESHOLD = 0.01f;
	
    float rotation = targetOrientation - tank->getAngle();
    rotation = MathUtil::mapToRange(rotation);
	float rotationSize = fabs(rotation);
    
    if (rotationSize < DISTANCE_THRESHOLD)
    {
        return 0.0f;
    }
    
	rotation /= DECELERATION;
    return MathUtil::clamp(rotation, -Tank::MAX_ROTATION, Tank::MAX_ROTATION);
}

float TankAI::lookForward(const Vec2f& velocity)
{
	if (velocity.lengthSquared() < MathUtil::ROUNDING_ERROR)
	{
		return 0.0f;
	}
	
	return align(atan2(-velocity.x, velocity.y));
}

float TankAI::aim(float targetOrientation)
{
	const float DECELERATION = 0.5f;
    const float DISTANCE_THRESHOLD = 0.01f;
	
    float rotation = targetOrientation - tank->getAimingAngle();
    rotation = MathUtil::mapToRange(rotation);
	float rotationSize = fabs(rotation);
    
    if (rotationSize < DISTANCE_THRESHOLD)
    {
        return 0.0f;
    }
    
	rotation /= DECELERATION;
    return MathUtil::clamp(rotation, -Tank::MAX_ROTATION, Tank::MAX_ROTATION);
}

float TankAI::aimForward(const Vec2f& velocity)
{
	if (velocity.lengthSquared() < MathUtil::ROUNDING_ERROR)
	{
		return 0.0f;
	}
	
	return aim(atan2(-velocity.x, velocity.y));
}

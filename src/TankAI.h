#ifndef __TANK_AGENT_H__
#define __TANK_AGENT_H__

#include "GameObjectObserver.h"

class GameObject;
class Tank;
class WorldInterface;

/// Base class for a tank AI.
class TankAI:
    public GameObjectObserver
{
public:
	TankAI(Tank* _tank, WorldInterface* _worldInterface);
	
    virtual ~TankAI();

    
	/// Called on mouseDown. Can be used for testing purpose.
	virtual void mouseDown(int x, int y, bool altDown);
	
	virtual void update(float delta);
	
	virtual void debugDraw();
	
	/// Returns the tank which has to be controlled by the AI
	Tank* getTank()
	{
		return tank;
	}
	
	/// Returns the WorldInterface
	WorldInterface* getWorldInterface()
	{
		return worldInterface;
	}
    
    /// Called when a collision on the assoziated Tank happens.
    virtual void onCollision(GameObject* collObject)
    {}
	
	/// Steering behaviors
	/// Returs a steering linear toward the given target.
    Vec2f seek(const Vec2f& target);
    
    /// Returns a steering linear toward the given target.
    /// When close to target the steering linear will break.
    Vec2f arrive(const Vec2f& target);
    
    /// Returns a steering linear toward the future position of the target.
    Vec2f pursue(Tank* target);
	
	/// Returns a steering linear away the given position.
	Vec2f flee(const Vec2f& target);
	
	/// Returns a steering linear away from the future position of the target.
	Vec2f evade(Tank* target);
	
	/// Returns a steering angular to rotate to the given orientation.
	float align(float targetOrientation);
	
	/// Returns a steering angular to look to the given velocity.
	float lookForward(const Vec2f& velocity);
	
	/// Returns a steering aiming to rotate to the aiming orientation.
	float aim(float targetOrientation);
	
	/// Returns a steering aiming to aim to the given velocity.
	float aimForward(const Vec2f& velocity);
	
private:
	Tank* tank;
	WorldInterface* worldInterface;
};

#endif
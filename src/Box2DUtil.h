#ifndef __BOX2D_UTIL__
#define __BOX2D_UTIL__

#include "Box2D/Box2D.h"

/// Utility class for converting between Cinder and Box2D classes.
class Box2DUtil
{
public:
	/// Converts a Box2D Vector to Cinder.
	static Vec2f convert(const b2Vec2& vec)
	{
		return Vec2f(vec.x, vec.y);
	}
	
	/// Converts a Cinder Vector to Box2D.
	static b2Vec2 convert(const Vec2f& vec)
	{
		return b2Vec2(vec.x, vec.y);
	}
};

#endif

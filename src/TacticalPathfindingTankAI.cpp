
#include "stdafx.h"
#include "GameDefines.h"

#include "TacticalPathfindingTankAI.h"
#include "WorldInterface.h"
#include "Tank.h"
#include "MathUtil.h"
#include "StringUtil.h"
#include "DrawingUtil.h"
#include "WalkToAction.h"
#include "NavigationGraph.h"

TacticalPathfindingTankAI::TacticalPathfindingTankAI(Tank* _tank, WorldInterface* _worldInterface):
	TankAI(_tank, _worldInterface),
	state(STATE_SELECT_TARGET)
{

	target = Vec2f(100.0f, 500.0f);
	
	walkToAction = new WalkToAction(this);
    
}

TacticalPathfindingTankAI::~TacticalPathfindingTankAI()
{
	delete walkToAction;
}



void TacticalPathfindingTankAI::update(float delta)
{	
	Tank* myTank = getTank();
	
	if (state == STATE_FOLLOW_PATH)
	{
		walkToAction->update(delta);
	}
	else
	{
		NavigationGraph* graph = World::getInstance()->getNavigationGraph();
		
		path.clear();
		graph->calcPath(myTank->getPosition(), target, path, -1000);
	
		if (state == STATE_PREPARE_PATH)
		{
			walkToAction->setWaypoints(path);
			state = STATE_FOLLOW_PATH;
		}
	}
}

void TacticalPathfindingTankAI::debugDraw()
{
}

void TacticalPathfindingTankAI::mouseDown(int x, int y, bool altPressed)
{
	target.set(x, y);

	/*if (altPressed)
	{
		state = STATE_PREPARE_PATH;
	}*/
}
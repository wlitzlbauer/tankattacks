#include "stdafx.h"
#include "GameDefines.h"
#include "FindPathAction.h"
#include "TankAI.h"
#include "Tank.h"
#include "World.h"
#include "NavigationGraph.h"

FindPathAction::FindPathAction(TankAI* _ai):
	Action(_ai),
	target(NULL)
{
}

void FindPathAction::update(float delta)
{
    Tank* myTank = getTank();
	
	NavigationGraph* graph = World::getInstance()->getNavigationGraph();
	
	std::vector<Vec2f> path;
	graph->calcPathDijkstra(myTank->getPosition(), target->getPosition(), path);
}

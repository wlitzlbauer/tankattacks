
#ifndef __PURSUE_ACTION_H__
#define __PURSUE_ACTION_H__

#include "Action.h"

class TankAI;
class Tank;

class PursueAction:
    public Action
{
public:
    PursueAction(TankAI* _ai, Tank* _evader = NULL);
    
    void setEvader(Tank* _evader)
    {
        evader = _evader;
    }
    
    virtual void update(float delta);
	
	virtual const char* getDebugName()
	{
		return "PursueAction";
	}
    
private:
    Tank* evader;
};

#endif


#ifndef __FLEE_ACTION_H__
#define __FLEE_ACTION_H__

#include "Action.h"

class TankAI;
class Tank;

class FleeAction:
public Action
{
public:
    FleeAction(TankAI* _ai);
    
    void setPursuer(Tank* _pursuer)
    {
        pursuer = _pursuer;
    }
    
    virtual void update(float delta);
	
	virtual const char* getDebugName()
	{
		return "FleeAction";
	}
    
private:
    Tank* pursuer;
};

#endif
#ifndef __NODE_H__
#define __NODE_H__

#include <vector>

#include "Connection.h"


class Node
{
	friend class NavigationGraph;
	
public:	
	static const float NODE_SIZE;
	static const float NODE_SIZE_HALF;
	
	Node(const Vec2i& _gridIdx, const Vec2f& _center);
	
	void debugDraw() const;
	
	/// Adds a connection to the node.
	void addConnection(const Connection& target);
	
	/// Returns a list of connection going away from this node.
	const std::vector<Connection>& getConnections()
	{
		return connections;
	}
	
	/// Returns the center of the node in world space.
	const Vec2f& getCenter() const
	{
		return center;
	}
	
	/// Add influence to the node.
	void addInfluence(float delta)
	{
		influence += delta;
	}
	
	/// Sets the influence of the node.
	void setInfluence(float _influence)
	{
		influence = _influence;
	}
	
	/// Returns the influence value of this node.
	float getInfluence() const
	{
		return influence;
	}
	
private:
	/// All connection going away from this node.
	std::vector<Connection> connections;
	
	/// The 2d index of the node in the grid.
	Vec2i gridIdx;
	
	/// The center of the node in world space.
	Vec2f center;
	
	/// Stores the influence of this node.
	float influence;
};


#endif
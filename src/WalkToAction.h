
#ifndef __WALK_TO_ACTION_H__
#define __WALK_TO_ACTION_H__

#include "Action.h"

class WalkToAction:
    public Action
{
public:
    WalkToAction(TankAI* _ai);
    
    virtual void onActivated();
    
    virtual void update(float delta);
    
    virtual void debugDraw();
	
	virtual const char* getDebugName()
	{
		return "WalkToAction";
	}
	
	void setWaypoints(const std::vector<Vec2f>& newPath);
    
private:
    int getClosestWaypoint(const Vec2f& position);
    
    std::vector<Vec2f> waypoints;
    int currentIdx;
};

#endif

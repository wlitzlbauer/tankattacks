#include "stdafx.h"
#include "GameDefines.h"
#include "Node.h"
#include "Connection.h"
#include "MathUtil.h"

const float Node::NODE_SIZE = 50.0f;
const float Node::NODE_SIZE_HALF = 25.0f;

Node::Node(const Vec2i& _gridIdx, const Vec2f& _center):
	gridIdx(_gridIdx),
	center(_center),
	influence(0.0f)
{}

void Node::addConnection(const Connection& target)
{
	connections.push_back(target);
}

void drawArraw(const Vec2f& a, const Vec2f& b, const float arrowSize = 2.5f)
{
	Vec2f dir = a - b;
	dir.normalize();
	Vec2f up(dir.y, -dir.x);
	gl::drawLine(a, b);
	//gl::drawLine(b, b + dir * arrowSize + up * arrowSize);
	//gl::drawLine(b, b + dir * arrowSize - up * arrowSize);
}

void Node::debugDraw() const
{
	Rectf rect(Vec2f(-NODE_SIZE_HALF, -NODE_SIZE_HALF), Vec2f(NODE_SIZE_HALF, NODE_SIZE_HALF));
	rect.offset(Vec2f(center.x, center.y));
	gl::color(0.5f, 0.5f, 0.5f);
	//gl::drawStrokedRect(rect);
	
	gl::color(1.0f, 1.0f, 1.0f);
	
	for (int i = 0; i < (int) connections.size(); i++)
	{
		Connection connection = connections[i];
	
		Vec2f dir = connection.getToNode()->center - center;
		dir.normalize();
		Vec2f a = center + dir * 10.0f;
		Vec2f b = center + dir * 40.0f;
		
		if (influence < 0.0f)
		{
			gl::color(0.0f, 0.0f, MathUtil::max(-influence, -1.0f));
		}
		else
		{
			gl::color(MathUtil::min(influence, 1.0f), 0.0f, 0.0f);
		}
		
		gl::drawSolidCircle(center, 5.0f);
		
		gl::color(0.5f, 0.5f, 0.5f);
		drawArraw(a, b);
	}
}
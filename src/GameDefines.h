#ifndef __GAME_DEFINES_H__
#define __GAME_DEFINES_H__

// include all global stuff
// -> use as precompiled header for compile time speed up

#include <string>
#include "cinder/Cinder.h"
#include "cinder/gl/gl.h"
#include "cinder/app/AppBasic.h"

using namespace ci;
using namespace ci::app;

struct CollisionContact
{
	Vec2f point;
	Vec2f normal;
};

#endif
#include "stdafx.h"
#include "GameDefines.h"
#include "WalkToAction.h"

#include "TankAi.h"
#include "Tank.h"
#include "MathUtil.h"

WalkToAction::WalkToAction(TankAI* _ai):
    Action(_ai),
    currentIdx(0)
{}

void WalkToAction::onActivated()
{
    currentIdx = getClosestWaypoint(getAI()->getTank()->getPosition());   
}

void WalkToAction::update(float delta)
{
    const float PATROL_SPEED = 30.0f;
    
    TankAI* ai = getAI();
    Tank* tank = ai->getTank();
    
    Vec2f currentWaypoint = waypoints[currentIdx];
    Vec2f distanceToWaypoint = currentWaypoint - tank->getPosition();
    
    if (distanceToWaypoint.length() < 50.0f)
    {
        currentIdx = MathUtil::min(currentIdx + 1, (int) waypoints.size() -1);
        currentWaypoint = waypoints[currentIdx];
    }

	Tank::Steering steering;
	
	if (currentIdx == waypoints.size() -1)
	{
		steering.linear = ai->arrive(currentWaypoint);
	}
	else
	{
		steering.linear = ai->seek(currentWaypoint);
	}

	steering.angular = ai->lookForward(tank->getLinearVelocity() + steering.linear);
	steering.aiming = ai->aimForward(tank->getLinearVelocity() + steering.linear);
	
	tank->setSteering(steering);
}

void WalkToAction::setWaypoints(const std::vector<Vec2f>& newPath)
{
	waypoints.resize(newPath.size());
	std::copy(newPath.begin(), newPath.end(), waypoints.begin() );
}

int WalkToAction::getClosestWaypoint(const Vec2f& position)
{
    float closestDistance = 1000000.0f;
    int result = 0;
    
    for (size_t i = 0; i < waypoints.size(); i++)
    {
        Vec2f distance = waypoints[i] - position;
        float d = distance.lengthSquared();
        
        if (d < closestDistance)
        {
            closestDistance = d;
            result = i;
        }
    }
    
    return result;
}

void WalkToAction::debugDraw()
{
    if (waypoints.size() > 0)
    {
        Vec2f lastPoint = waypoints.back();
        
        for (size_t i = 0; i < waypoints.size(); i++)
        {
            Vec2f currentPoint = waypoints[i];
            gl::drawLine(lastPoint, currentPoint);
			
			if (i == currentIdx)
			{
				gl::color(255, 0, 0);
			}
            gl::drawStrokedCircle(currentPoint, 3.0f);
            lastPoint = currentPoint;
			gl::color(255, 255, 255);
        }
    }
}
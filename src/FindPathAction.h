
#ifndef __FIND_PATH_ACTION_H__
#define __FIND_PATH_ACTION_H__

#include "Action.h"

class TankAI;
class Tank;

class FindPathAction:
	public Action
{
public:
    FindPathAction(TankAI* _ai);
    
    void setTarget(Tank* _target)
    {
        target = _target;
    }
    
    virtual void update(float delta);
	
	virtual const char* getDebugName()
	{
		return "FindPathAction";
	}
    
private:
    Tank* target;
};

#endif
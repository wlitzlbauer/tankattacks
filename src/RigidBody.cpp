#include "stdafx.h"
#include "GameDefines.h"

#include "RigidBody.h"

RigidBody::~RigidBody()
{
	if (body != NULL)
	{
		body->GetWorld()->DestroyBody(body);
	}
}

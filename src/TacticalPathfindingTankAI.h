#ifndef __TACTICAL_PATHFINDING_TANK_AGENT_H__
#define __TACTICAL_PATHFINDING_TANK_AGENT_H__

#include "TankAI.h"

class Tank;
class WorldInterface;
class WalkToAction;


/// TankAI used to demonstrate tactical pathfinding
class TacticalPathfindingTankAI:
	public TankAI
{
public:
	TacticalPathfindingTankAI(Tank* _tank, WorldInterface* _worldInterface);
    
    virtual ~TacticalPathfindingTankAI();
	
	virtual void update(float delta);
	
	virtual void debugDraw();
    
	virtual void mouseDown(int x, int y, bool altDown);
	
private:
	enum State
	{
		STATE_SELECT_TARGET,
		STATE_PREPARE_PATH,
		STATE_FOLLOW_PATH
	};
	
	Vec2f target;
	std::vector<Vec2f> path;
	
	State state;
	WalkToAction* walkToAction;
};

#endif
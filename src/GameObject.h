#ifndef __GAME_OBJECT_H__
#define __GAME_OBJECT_H__


class GameObjectObserver;
class RigidBody;

/// Base class for all objects in the game world
/// GameObject must be registered to the World.
class GameObject
{
public:
	virtual ~GameObject();
	
	/// Returns the current position of the GameObject.
	const Vec2f& getPosition() const
	{
		return position;
	};
	
	/// Sets the current position of the GameObject.
	void setPosition(const Vec2f& _position);
	
	/// Returns the current angle of the GameObject.
	float getAngle() const
	{
		return angle;
	};
	
	/// Returns the current velocity of the object.
	/// Note: retrns zero if object doesn't have a RigidBody.
	Vec2f getLinearVelocity() const;
	
	/// Sets the current angle of the GameObject.
	void setAngle(float _angle);
	
	/// Returns the heading direction of the GameObject.
	/// Note: Vector is calculated from the current orientation.
	Vec2f getDirection() const
	{
		return Vec2f(-sin(angle), cos(angle));
	}
	
	/// Returns the radius of the GameObject
	float getRadius() const
	{
		return radius;
	}
	
	/// Sets the radius of the GameObject
	void setRadius(float _radius)
	{
		radius = _radius;
	}
	
	const std::string& getName() const
	{
		return name;
	}
	
	void setName(const std::string& _name)
	{
		name = _name;
	}
    
	/// Adds a ovserver to the GameObject
    void addObserver(GameObjectObserver* observer);
	
	/// Updates the GameObject
	/// Note: - Called every frame by the World
	///       - Parent update() should by child class.
	virtual void update(float delta);
	
	/// Draws the GameObject
	/// Note: Only overwrite if needed. Use drawElement instead.
	virtual void draw() const;
	
	/// Draws the contect of the GameObject.
	/// Note: Called by draw() after position and angle
	///       is applied to the model-view-matrix
	virtual void drawElement() const;
	
	/// Draws debug information of the GameObject.
	/// Note: Position and angle is not applied to the
	///       model-view-mtrix.
	virtual void debugDraw() const;
	
	/// Called when the GameObjects collides with another object.
	virtual void onCollision(GameObject* collObjects);
	
	/// Returns the RigidBody of this GameObject.
	RigidBody* getRigidBody()
	{
		return rigidBody;
	}
	
	const RigidBody* getRigidBody() const
	{
		return rigidBody;
	}
	
	/// Sets the RigidBody of this GameObject.
	void setRigidBody(RigidBody* body);
	
protected:
	GameObject();
	
private:
	/// static counter used for automatic naming of objects.
	static int gameObjectCounter;
	
	/// The name of the GameObject
	std::string name;
	
	/// Position of the GameObject
	Vec2f position;
	
	/// Angle of the GameObject in radian
	float angle;
	
	/// The radius of the GameObject.
	float radius;
	
	/// The rigid body of the GameObject.
	RigidBody* rigidBody;
    
    std::vector<GameObjectObserver*> observers;
};

#endif


#include "stdafx.h"
#include "GameDefines.h"

#include "HumanController.h"
#include "MathUtil.h"
#include "Tank.h"

HumanController::HumanController(Tank* _tank):
	tank(_tank),
	move(0),
	steer(0),
    fire(0),
	aiming(0),
	aimingOffset(0)
{
}

void HumanController::keyDown(KeyEvent event)
{
	switch (event.getCode())
	{
		case KeyEvent::KEY_LEFT:
			steer = -1;
			break;
		case KeyEvent::KEY_RIGHT:
			steer = 1;
			break;
		case KeyEvent::KEY_UP:
			move = 1;
			break;
		case KeyEvent::KEY_DOWN:
			move = -1;
			break;
        case KeyEvent::KEY_SPACE:
            fire = 1;
            break;
		case KeyEvent::KEY_a:
			aiming = -1;
			break;
		case KeyEvent::KEY_d:
			aiming = 1;
			break;
	}
}

void HumanController::keyUp(KeyEvent event)
{
	switch (event.getCode())
	{
		case KeyEvent::KEY_LEFT:
		case KeyEvent::KEY_RIGHT:
			steer = 0;
			break;
		case KeyEvent::KEY_UP:
		case KeyEvent::KEY_DOWN:
			move = 0;
			break;
		case KeyEvent::KEY_a:
		case KeyEvent::KEY_d:
			aiming = 0;
			break;
	}
}

float aim(float targetOrientation, float currentOrientation)
{
	const float DECELERATION = 0.001f;
    const float DISTANCE_THRESHOLD = 0.01f;
	
    float rotation = targetOrientation - currentOrientation;
    rotation = MathUtil::mapToRange(rotation);
	float rotationSize = fabs(rotation);
    
    if (rotationSize < DISTANCE_THRESHOLD)
    {
        return 0.0f;
    }
    
	rotation /= DECELERATION;
    return MathUtil::clamp(rotation, -Tank::MAX_ROTATION, Tank::MAX_ROTATION);
}

void HumanController::update(float delta)
{
    if (fire > 0)
    {
        tank->fire();
        fire = 0;
    }
	Vec2f desiredVelocity = tank->getDirection() * Tank::MAX_SPEED * move;
	
	Tank::Steering steering;
	steering.linear = desiredVelocity - tank->getLinearVelocity();
	steering.angular =  Tank::MAX_ROTATION * steer;
	
	float angle = tank->getAngle();
	aimingOffset += aiming * delta * Tank::MAX_ROTATION;
	
	steering.aiming = aim(angle + aimingOffset, tank->getAimingAngle());
	
	tank->setSteering(steering);
}
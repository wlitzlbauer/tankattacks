#ifndef __WALL_H__
#define __WALL_H__

#include "GameObject.h"

/// Represents a wall obstacle in the game world.
/// Note: currently only horizontal and vertical walls are supported.
class Wall:
	public GameObject
{
public:
	Wall(const Vec2f& start, const Vec2f& end, float size);
	
	virtual void drawElement() const;
	
private:
	Rectf bounds;
};

#endif
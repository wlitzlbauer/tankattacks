#include "stdafx.h"
#include "GameDefines.h"
#include "NavigationGraph.h"
#include "Node.h"
#include "World.h"
#include "MathUtil.h"
#include "Connection.h"
#include "PathfindingList.h"
#include "Tank.h"


void NavigationGraph::DebugDrawer::clear()
{
	open.clear();
	closed.clear();
	path.clear();
}

void NavigationGraph::DebugDrawer::update(const PathfindingList& openList, const PathfindingList& closedList, const std::vector<Vec2f>& newPath)
{
	clear();

	
	for (int i = 0; i < (int) openList.nodes.size(); i++)
	{
		open.push_back(openList.nodes[i].node->getCenter());
	}
	
	for (int i = 0; i < (int) closedList.nodes.size(); i++)
	{
		closed.push_back(closedList.nodes[i].node->getCenter());
	}
	
	path.resize(newPath.size());
	std::copy(newPath.begin(), newPath.end(), path.begin() );
}

void NavigationGraph::DebugDrawer::debugDraw() const
{
	/*for (int i = 0; i < (int) open.size(); i++)
	{
		gl::color(255, 0, 0);
		gl::drawStrokedCircle(open[i], 8);
	}
	
	for (int i = 0; i < (int) closed.size(); i++)
	{
		gl::color(255, 255, 255);
		gl::drawStrokedCircle(closed[i], 8);
	}*/
	
	if (path.size() > 0)
	{
		gl::lineWidth(3.0f);
		Vec2f p = path[0];
		
		for (int i = 1; i < (int) path.size(); i++)
		{
			gl::color(0, 255, 0);
			gl::drawLine(p, path[i]);
			p = path[i];
		}
		gl::lineWidth(1.0f);
	}
	
	gl::color(255, 255, 255);
}

NavigationGraph::NavigationGraph(int _width, int _height):
	width(_width),
	height(_height)
{
	gridWidth = (int) floor(width / Node::NODE_SIZE);
	gridHeight = (int) floor(height / Node::NODE_SIZE);
	
	int gridSize = gridWidth * gridHeight;
	grid.reserve(gridSize);
	
	for (int i = 0; i < gridSize; i++)
	{
		grid.push_back(NULL);
	}
}


NavigationGraph::~NavigationGraph()
{
	for (size_t i = 0; i < grid.size(); i++)
	{
		delete grid[i];
	}

	grid.clear();
}


void NavigationGraph::initGraph()
{
	World* world = World::getInstance();
	std::vector<GameObject*> obj;
	
	for (int y = 0; y < gridHeight; y++)
	{
		for (int x = 0; x < gridWidth; x++)
		{
			Vec2f pos(x * Node::NODE_SIZE + Node::NODE_SIZE_HALF,
					  y * Node::NODE_SIZE + Node::NODE_SIZE_HALF);
			
			Rectf rect(-Node::NODE_SIZE_HALF + 5, -Node::NODE_SIZE_HALF + 5,
					   Node::NODE_SIZE_HALF - 5, Node::NODE_SIZE_HALF - 5);
			
			// Check if there obstacles.
			if (world->queryAABB(pos, rect))
			{
				continue;
			}
			
			Node* node = new Node(Vec2i(x, y), pos);
			Node* leftNode = getNode(x-1, y);
			Node* topNode = getNode(x, y-1);
			
			if (leftNode != NULL)
			{
				Vec2f distance = node->getCenter() - leftNode->getCenter();
				
				if (!world->rayCast(node->getCenter(), leftNode->getCenter()))
				{
					float cost = distance.length();
					node->connections.push_back(Connection(node, leftNode, cost));
					leftNode->connections.push_back(Connection(leftNode, node, cost));
				}
			}
			if (topNode != NULL)
			{
				Vec2f distance = node->center - topNode->center;
				
				if (!world->rayCast(node->center, topNode->center))
				{
					float cost = distance.length();
					node->connections.push_back(Connection(node, topNode, cost));
					topNode->connections.push_back(Connection(topNode, node, cost));
				}
			}

			grid[x + y * gridWidth] = node;
		}
	}
}


Node* NavigationGraph::getNodeAt(const Vec2f& position)
{
	int idxX = (int) MathUtil::round((position.x - Node::NODE_SIZE_HALF) / Node::NODE_SIZE);
	int idxY = (int) MathUtil::round((position.y - Node::NODE_SIZE_HALF) / Node::NODE_SIZE);
	
	idxX = MathUtil::clamp(idxX, 0, gridWidth);
	idxY = MathUtil::clamp(idxY, 0, gridHeight);
	
	return getNode(idxX, idxY);
}

Node* NavigationGraph::getNode(int x, int y)
{
	if ((x >= 0) && (x < gridWidth) && (y >= 0) && (y < gridHeight))
	{
		return grid[x + y * gridWidth];
	}
	else
	{
		return NULL;
	}
}

void NavigationGraph::debugDraw() const
{
	for (int i = 0; i < (int) grid.size(); i++)
	{
		if (grid[i] != NULL)
		{
			grid[i]->debugDraw();
		}
	}
	
	debugDrawer.debugDraw();
}


bool NavigationGraph::calcPathDijkstra(const Vec2f& currentPosition, const Vec2f& targetPosition, std::vector<Vec2f>& path, float influenceWeight)
{
	Node* start = getNodeAt(currentPosition);
	Node* goal = getNodeAt(targetPosition);
	
	if ((start == NULL) || (goal == NULL))
	{
		return false;
	}
	
	PathfindingList open;
	PathfindingList closed;
	
	PathfindingList::NodeRecord startRecord;
	startRecord.node = start;
	startRecord.connection = Connection(NULL, NULL, 0);
	startRecord.costSoFar = 0;
	
	open.add(startRecord);
	
	PathfindingList::NodeRecord current;
	
	while (!open.isEmpty())
	{
		// get the closest node on the open list.
		current = open.getSmallestPerCostSoFar();
		
		if (current.node == goal)
		{
			// goal found -> terminate
			break;
		}
		
		const std::vector<Connection>& connections = current.node->getConnections();
		
		// loop through the connections
		for (int i = 0; i < (int) connections.size(); i++)
		{
			Connection connection = connections[i];
			
			Node* endNode = connection.getToNode();
			float costSoFar = current.costSoFar + calcCost(connection, influenceWeight);
			PathfindingList::NodeRecord endNodeRecord;	// initially empty
			
			if (closed.contains(endNode))
			{
				// skip node if its already closed
				continue;
			}
			else if (open.contains(endNode))
			{
				// node already on open list.
				endNodeRecord = open.find(endNode);
				
				if (endNodeRecord.costSoFar <= costSoFar)
				{
					// if new route to node isn't better, then skip.
					continue;
				}
			}
			else
			{
				endNodeRecord.node = endNode;
			}
			
			// update node
			endNodeRecord.costSoFar = costSoFar;
			endNodeRecord.connection = connection;
			
			
			if (!open.contains(endNode))
			{
				// add node if not already on open list.
				open.add(endNodeRecord);
			}
		}
		
		// remove node from open and mark it as already visited.
		open.remove(current.node);
		closed.add(current);
	}

	
	if (current.node != goal)
	{
		// no path found
		return false;
	}
	
	while (current.node != start)
	{
		// travel back the records to accumulate final path
		path.push_back(current.node->getCenter());
		current = closed.find(current.connection.getFromNode());
	}
	
	path.push_back(start->getCenter());
	
	// path must be reversed to begin with the start node.
	std::reverse(path.begin(), path.end());
	
	// update debugDrawer with open list, closed list and path to debugDrawer for debug rendering.
	debugDrawer.update(open, closed, path);
	
	return true;
}



bool NavigationGraph::calcPath(const Vec2f& currentPosition, const Vec2f& targetPosition, std::vector<Vec2f>& path, float influenceWeight)
{
	Node* start = getNodeAt(currentPosition);
	Node* goal = getNodeAt(targetPosition);
	
	if ((start == NULL) || (goal == NULL))
	{
		return false;
	}
	
	PathfindingList open;
	PathfindingList closed;
	
	PathfindingList::NodeRecord startRecord;
	startRecord.node = start;
	startRecord.connection = Connection(NULL, NULL, 0);
	startRecord.costSoFar = 0;
	startRecord.estimatedTotalCost = heuristic(start, goal);
	
	open.add(startRecord);
	
	PathfindingList::NodeRecord current;
	
	while (!open.isEmpty())
	{
		// get the closest node on the open list.
		current = open.getSmallest();
		
		if (current.node == goal)
		{
			// goal found -> terminate
			break;
		}
		
		const std::vector<Connection>& connections = current.node->getConnections();
		
		// loop through the connections
		for (int i = 0; i < (int) connections.size(); i++)
		{
			Connection connection = connections[i];
			
			Node* endNode = connection.getToNode();
			float costSoFar = current.costSoFar + calcCost(connection, influenceWeight);
			PathfindingList::NodeRecord endNodeRecord;	// initially empty
			
			if (closed.contains(endNode))
			{
				endNodeRecord = closed.find(endNode);

				if (endNodeRecord.costSoFar <= costSoFar)
				{
					continue;
				}

				closed.remove(endNode);
			}
			else if (open.contains(endNode))
			{
				// node already on open list.
				endNodeRecord = open.find(endNode);
				
				if (endNodeRecord.costSoFar <= costSoFar)
				{
					// if new route to node isn't better, then skip.
					continue;
				}
			}
			else
			{
				endNodeRecord.node = endNode;
			}
			
			// update node
			endNodeRecord.costSoFar = costSoFar;
			endNodeRecord.estimatedTotalCost = costSoFar + heuristic(endNodeRecord.node, goal);
			endNodeRecord.connection = connection;
			
			
			if (!open.contains(endNode))
			{
				// add node if not already on open list.
				open.add(endNodeRecord);
			}
		}
		
		// remove node from open and mark it as already visited.
		open.remove(current.node);
		closed.add(current);
	}

	
	if (current.node != goal)
	{
		// no path found
		return false;
	}
	
	while (current.node != start)
	{
		// travel back the records to accumulate final path
		path.push_back(current.node->getCenter());
		current = closed.find(current.connection.getFromNode());
	}
	
	path.push_back(start->getCenter());
	
	// path must be reversed to begin with the start node.
	std::reverse(path.begin(), path.end());
	
	// update debugDrawer with open list, closed list and path to debugDrawer for debug rendering.
	debugDrawer.update(open, closed, path);
	
	return true;
}

void NavigationGraph::resetInfluenceMap()
{
	for (int i = 0; i < (int) grid.size(); i++)
	{
		if (grid[i] != NULL)
		{
			grid[i]->setInfluence(0.0f);
		}
	}
}

float NavigationGraph::heuristic(Node* start, Node* goal)
{
	return start->getCenter().distance(goal->getCenter());
}

float NavigationGraph::calcCost(const Connection& connection, float influenceWeight)
{
	// TODO update cost funcion for tactical pathfinding
	return connection.getCost();
}

void NavigationGraph::updateInfluenceMap(std::vector<Tank*>& tanks)
{
	resetInfluenceMap();
	
	// TODO fill influence map with values
}

#include "stdafx.h"
#include "GameDefines.h"
#include "DrawingUtil.h"

#include "BehaviorTreeTankAI.h"
#include "BehaviorTree.h"
#include "Tank.h"

#include "PatrolAction.h"
#include "PursueAction.h"
#include "WorldInterface.h"

BehaviorTreeTankAI::BehaviorTreeTankAI(Tank* _tank, WorldInterface* _worldInterface):
	TankAI(_tank, _worldInterface)
{
	Tank* player = getWorldInterface()->getPlayerTank();
}

BehaviorTreeTankAI::~BehaviorTreeTankAI()
{
	//delete behaviorTree;
}

void BehaviorTreeTankAI::update(float delta)
{
	//behaviorTree->tick(delta);
}
#include "stdafx.h"
#include "GameDefines.h"
#include "Wall.h"
#include "World.h"

#include "RigidBody.h"
#include "MathUtil.h"

Wall::Wall(const Vec2f& start, const Vec2f& end, float size)
{
	Vec2f direction = end - start;
	Vec2f right(direction);
	Vec2f center = (start + end) / 2;
	right.rotate(MathUtil::PI_HALF);
	
	direction.normalize();
	right.normalize();
	
	direction *= size;
	right *= size;
	
	std::vector<Vec2f> p(2);
	p[0] = start - direction - right - center;
	p[1] = end + direction + right - center;
	
	bounds = Rectf(p);

	setRadius(bounds.getCenter().distance(bounds.getUpperLeft()));
	setRigidBody(World::getInstance()->createStaticBox(Vec2f::zero(), (bounds.getSize() / 2)));
	
	setPosition(center);
}


void Wall::drawElement() const
{
	gl::drawSolidRect(bounds);
}


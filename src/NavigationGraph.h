#ifndef __NAVIGATION_GRAPH_H__
#define __NAVIGATION_GRAPH_H__

#include <vector>

class Node;
class Connection;
class PathfindingList;
class Tank;

/// A simple grid based navigation graph used for pathfinding.
class NavigationGraph
{
public:
	/// Helper class to render the last found path as debug draw.
	class DebugDrawer
	{
	public:
		void clear();
		void update(const PathfindingList& openList, const PathfindingList& closedList, const std::vector<Vec2f>& newPath);
		void debugDraw() const;
		
	private:
		std::vector<Vec2f> open;
		std::vector<Vec2f> closed;
		std::vector<Vec2f> path;
	};
	
	NavigationGraph(int _width, int _height);
	virtual ~NavigationGraph();
	
	/// calculates the navigation grid.
	/// Note: should be called before dynamic objects are created in the world.
	void initGraph();
	
	/// clears the current influence map.
	void resetInfluenceMap();
	
	/// updates the influence map with the given tanks.
	void updateInfluenceMap(std::vector<Tank*>& tanks);
	
	void debugDraw() const;
	
	/// returns a node at the given position.
	Node* getNodeAt(const Vec2f& position);
	
	bool calcPathDijkstra(const Vec2f& currentPosition, const Vec2f& targetPosition, std::vector<Vec2f>& path, float influenceWeight = 0);
	bool calcPath(const Vec2f& currentPosition, const Vec2f& targetPosition, std::vector<Vec2f>& path, float influenceWeight = 0.0f);
	
private:
	float heuristic(Node* start, Node* goal);
	float calcCost(const Connection& connection, float influenceWeight);
	Node* getNode(int x, int y);
	
	int width;
	int height;
	int gridWidth;
	int gridHeight;
	std::vector<Node*> grid;
	
	DebugDrawer debugDrawer;
};

#endif